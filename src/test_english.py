# coding=utf-8
import re

from sklearn.decomposition.truncated_svd import TruncatedSVD
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model.stochastic_gradient import SGDClassifier
from sklearn.multiclass import OneVsRestClassifier, OutputCodeClassifier
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.neighbors.unsupervised import NearestNeighbors
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.svm.classes import SVC
from sklearn.tree import DecisionTreeClassifier

from src.classifier_3cgp.adapters.tag_text_adapter_filer_corpus_adapter import TagTextAdapterFilterCorpusAdapter
from src.classifier_3cgp.adapters.lemmatizer_corpus_adapter import LemmatizerCorpusAdapter, WordNetLemmatizer
from src.classifier_3cgp.adapters.stemmer_corpus_adapter import EnglishStemmerCorpusAdapter
from src.classifier_3cgp.feature_selection.bi_normal_separation import bns
from src.classifier_3cgp.feature_selection.cube_mutual_information import mi3
from src.classifier_3cgp.feature_selection.mutual_information import mi
from src.classifier_3cgp.tester_english import Tester



# SVM (Support vector machines) :       http://scikit-learn.org/stable/modules/svm.html
# SGD (Stochastic Gradient Descent) :   http://scikit-learn.org/stable/modules/sgd.html
# NN  (Nearest Neighbors) :             http://scikit-learn.org/stable/modules/neighbors.html
# NB  (Naive Bayes) :                   http://scikit-learn.org/stable/modules/naive_bayes.html
# DT  (Decision Tree) :                 http://scikit-learn.org/stable/modules/tree.html
# RF  (Random Forest) :                 http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
from src.classifier_3cgp.utils.dense_transformer import DenseTransformer

__author__ = 'Sébastien Rigaux'

#region --- Corpus Adapters ---------------------------------------------------

# tag_filter_regex = re.compile("Q[CDEHPR]\d+.*")
tag_filter_regex = re.compile("Q.*")
bests_tag_filter_regex = re.compile("Q[CDPRST].*")
only_words = re.compile("^\w*$")
only_char_regex = re.compile("^[a-zA-ZÀ-ÿ]*$")

default_corpus_adapter =           TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s, tag_filter=lambda s: tag_filter_regex.match(s))
bests_tag_filter_corpus_adapter =  TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s, tag_filter=lambda s: bests_tag_filter_regex.match(s))
only_2alpha_tag_corpus_adapter =   TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s[:2], tag_filter=lambda s: tag_filter_regex.match(s))
only_3alpha_tag_corpus_adapter =   TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s[:3], tag_filter=lambda s: tag_filter_regex.match(s))

stemmer_corpus_adapter =           EnglishStemmerCorpusAdapter()
lemmatizer_corpus_adapter =        LemmatizerCorpusAdapter(WordNetLemmatizer())

lemmatizer_filtered_corpus_adapter = LemmatizerCorpusAdapter(WordNetLemmatizer(
    word_filter=lambda w: (only_words.match(w.word) and w.postag.startswith(('CD', 'EX', 'FW', 'IN', 'J', 'MD', 'N', 'PRP', 'R', 'TO', 'UH', 'V', 'W')))))

lemmatizer_filtered_corpus_adapter_2 = LemmatizerCorpusAdapter(WordNetLemmatizer(
    word_filter=lambda w: (only_char_regex.match(w.word) and w.postag.startswith(('FW', 'J', 'MD', 'N', 'R', 'V', 'W')))))

# http://www.cs.berkeley.edu/~klein/cs294-19/SP08%20cs294%20lecture%207%20--%20POS%20tagging%20(6PP).pdf

stop_words = Tester.stop_words()

#endregion

#region --- Vectorizers -------------------------------------------------------
count_vectorizer =           ('vectorizer', CountVectorizer(stop_words=stop_words))
tfidf_vectorizer_1gram =     ('vectorizer', TfidfVectorizer(ngram_range=(1, 1), stop_words=stop_words))
tfidf_vectorizer_2gram =     ('vectorizer', TfidfVectorizer(ngram_range=(1, 2), stop_words=stop_words))
tfidf_vectorizer_3gram =     ('vectorizer', TfidfVectorizer(ngram_range=(1, 3), stop_words=stop_words))
tfidf_vectorizer_2_3gram =   ('vectorizer', TfidfVectorizer(ngram_range=(2, 3), stop_words=stop_words))
tfidf_vectorizer_max_df_05 = ('vectorizer', TfidfVectorizer(stop_words=stop_words,
                                                            max_df=0.4,
                                                            sublinear_tf=False,
                                                            max_features=700))
#endregion

#region --- Others ------------------------------------------------------------
to_dense = ('to_dense', DenseTransformer())

#endregion

#region --- Classifiers -------------------------------------------------------
ovr_l_svc =          ('clf', OneVsRestClassifier(LinearSVC(), n_jobs=-1))
ovr_l_svc_w_auto =   ('clf', OneVsRestClassifier(LinearSVC(class_weight='auto'), n_jobs=-1))
ovr_l_svc_best =     ('clf', OneVsRestClassifier(LinearSVC(class_weight='auto', penalty='l2', loss='l2', dual=True, C=0.01), n_jobs=-1))
ovr_svc =            ('clf', OneVsRestClassifier(SVC(probability=True), n_jobs=-1))
ovr_svc_kernel_rbf = ('clf', OneVsRestClassifier(SVC(), n_jobs=-1))
ovr_nn =             ('clf', OneVsRestClassifier(NearestNeighbors(n_neighbors=2, algorithm='ball_tree'), n_jobs=-1))
ovr_g_nb =           ('clf', OneVsRestClassifier(GaussianNB(), n_jobs=-1))
ovr_m_nb =           ('clf', OneVsRestClassifier(MultinomialNB(alpha=0.01), n_jobs=-1))
ovr_b_nb =           ('clf', OneVsRestClassifier(BernoulliNB(), n_jobs=-1))
ovr_sgd =            ('clf', OneVsRestClassifier(SGDClassifier(), n_jobs=-1))
ovr_sgd_w_auto =     ('clf', OneVsRestClassifier(SGDClassifier(class_weight='auto'), n_jobs=-1))
ovr_dt =             ('clf', OneVsRestClassifier(DecisionTreeClassifier()))  # pas les sparse
ovr_rf =             ('clf', OneVsRestClassifier(RandomForestClassifier()))
ovr_ab =             ('clf', OneVsRestClassifier(AdaBoostClassifier(DecisionTreeClassifier(max_depth=2),
                                                                    n_estimators=10, learning_rate=1.5)))
occ_l_svc = ('clf', OutputCodeClassifier(LinearSVC(random_state=0), code_size=2, random_state=0, n_jobs=-1))

#endregion

#region --- Feature Identification --------------------------------------------
fi_chi2_100  = ('fi', SelectKBest(chi2, k=100))
fi_chi2_1000 = ('fi', SelectKBest(chi2, k=1000))

fi_bns_100   = ('fi', SelectKBest(bns, k=100))
fi_bns_1000  = ('fi', SelectKBest(bns, k=1000))

fi_mi_100  =   ('fi', SelectKBest(mi, k=100))
fi_mi_1000 =   ('fs', SelectKBest(mi, k=1000))

fi_mi3_100   = ('fi', SelectKBest(mi3, k=100))
fi_mi3_1000  = ('fi', SelectKBest(mi3, k=1000))

fi_tSVD_100  = ('fi', TruncatedSVD(n_components=100))
fi_tSVD_1000 = ('fi', TruncatedSVD(n_components=1000))

#endregion

#region --- Pipelines ---------------------------------------------------------
ovr_l_count_vectorizer_pipeline =                 Pipeline([count_vectorizer, ovr_l_svc])
ovr_l_svc_1gram_pipeline =                        Pipeline([tfidf_vectorizer_1gram, ovr_l_svc])
ovr_l_svc_2gram_pipeline =                        Pipeline([tfidf_vectorizer_2gram, ovr_l_svc])
ovr_l_svc_3gram_pipeline =                        Pipeline([tfidf_vectorizer_3gram, ovr_l_svc])
ovr_l_svc_2_3gram_pipeline =                      Pipeline([tfidf_vectorizer_2_3gram, ovr_l_svc])
ovr_l_svc_2gram_chi2_pipeline =                   Pipeline([tfidf_vectorizer_2gram, fi_chi2_1000, ovr_l_svc])
ovr_svc_1gram_pipeline =                          Pipeline([tfidf_vectorizer_1gram, ovr_svc])
ovr_g_nb_1gram_pipeline =                         Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_g_nb])
ovr_m_nb_1gram_pipeline =                         Pipeline([tfidf_vectorizer_1gram, ovr_m_nb])
ovr_b_nb_1gram_pipeline =                         Pipeline([tfidf_vectorizer_1gram, ovr_b_nb])
ovr_svc_kernel_rbf_1gram_pipeline =               Pipeline([tfidf_vectorizer_1gram, ovr_svc_kernel_rbf])
ovr_nn_1gram_pipeline =                           Pipeline([tfidf_vectorizer_1gram, ovr_nn])
ovr_rf_1gram_pipeline =                           Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_rf])
ovr_dt_pipeline =                                 Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_dt])
ovr_ab_pipeline =                                 Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_ab])
ovr_dt_count_vectorizer_pipeline =                Pipeline([count_vectorizer, to_dense, ovr_dt])
ovr_sgd_1gram_pipeline =                          Pipeline([tfidf_vectorizer_1gram, ovr_sgd])
ovr_sgd_max_df_05_pipeline =                      Pipeline([tfidf_vectorizer_max_df_05, ovr_sgd])
ovr_sgd_max_df_05_w_auto_pipeline =               Pipeline([tfidf_vectorizer_max_df_05, ovr_sgd_w_auto])
ovr_sgd_max_df_count_vectorizer_pipeline =        Pipeline([count_vectorizer, ovr_sgd_w_auto])
ovr_l_svc_w_auto_pipeline =                       Pipeline([tfidf_vectorizer_1gram, ovr_l_svc_w_auto])
ovr_l_svc_w_auto_count_vectorizer_pipeline =      Pipeline([count_vectorizer, ovr_l_svc_w_auto])
ovr_l_svc_w_auto_count_vectorizer_chi2_pipeline = Pipeline([count_vectorizer, fi_chi2_1000, ovr_l_svc_w_auto])
occ_l_svc_pipeline =                              Pipeline([tfidf_vectorizer_1gram, occ_l_svc])
best_pipeline =                                   Pipeline([tfidf_vectorizer_1gram, fi_tSVD_1000, ovr_l_svc_best])

#endregion

# force the program to perform all the tests
force = False

# the list of test to execute
testers = [

    # Window Size - NGrams

    Tester("../results/logs/english/__best.log",
           """Best""",
           document_adapters=[lemmatizer_filtered_corpus_adapter_2],
           pipeline=best_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer.log",
           """
                Par document
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer_stemmer.log",
           """
                Par document
                stemmer
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[stemmer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer_lemmatizer.log",
           """
                Par document
                lemmatizer
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag (2)
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/00_1gram_count_vectorizer_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (best tags only)
                lemmatizer filtered by postag (2)
                Count vectorizer
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_stemmer.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Stemmer
           """,
           document_adapters=[stemmer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_lemmatizer.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Lemmatizer
           """,
           document_adapters=[lemmatizer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_lemmatizer_filtered_by_postag.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),


    Tester("../results/logs/english/01_1gram_w_auto_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (bests tags only)
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer_lemmatizer.log",
           """
                Par document
                lemmatizer
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           document_adapters=[lemmatizer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (bests tags only)
                lemmatizer filtered by postag 2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/01_1gram_w_auto_count_vectorizer_chi2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
                Chi2
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_chi2_pipeline,
           force=force or False),


    Tester("../results/logs/english/01b_1gram.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD))
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram.log",
           """
                Par document
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram_lemmatizer.log",
           """
                Par document
                Lemmatizer
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (bests tags only)
                lemmatizer filtered by postag 2
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/02_2gram_chi2.log",
           """
                Par document
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
                Chi2
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_chi2_pipeline,
           force=force or False),

    Tester("../results/logs/english/03_3gram.log",
           """
                Par document
                Window Size - 1, 2 & 3gram
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_3gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/04_2-3gram.log",
           """
                Par document
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/04_2-3gram_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/04_2-3gram_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/04_2-3gram_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (best tags only)
                lemmatizer filtered by postag 2
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/11_per_quotation.log",
           """
                Par phrase
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/11_per_quotation_w_auto.log",
           """
                Par phrase
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/21_2alpha_codes.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           document_adapters=[only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/21_2alpha_codes_w_auto.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/21_2alpha_codes_w_auto_lemmatizer_filtered_by_postag.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                lemmatizer filtered by postag
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/21_2alpha_codes_w_auto_lemmatizer_filtered_by_postag_2.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                lemmatizer filtered by postag_2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/21_2alpha_codes_w_auto_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes (bests tags only)
                lemmatizer filtered by postag_2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, only_2alpha_tag_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/22_3alpha_codes.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/22_3alpha_codes_w_auto.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline,
           force=force or False),

    Tester("../results/logs/english/23_3alpha_codes_SGD.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD))
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_maxDF_05.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD) with max DF = 0.5 & sublinear_tf)
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_maxDF_05_w_auto.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_w_auto_pipeline,
           force=force or False),


    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer_stemmer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                stemmer
           """,
           document_adapters=[stemmer_corpus_adapter, only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer_lemmatizer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer
           """,
           document_adapters=[only_3alpha_tag_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer_lemmatizer_filterd_by_postag.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer_lemmatizer_filterd_by_postag_2.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/24_3alpha_codes_SGD_count_vectorizer_lemmatizer_filterd_by_postag_2_bests_only.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes (bests tags only)
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, only_3alpha_tag_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline,
           force=force or False),

    Tester("../results/logs/english/31_svc.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest kernel SVC
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_svc_1gram_pipeline,
           force=force or False),

    # Tester("../results/logs/english/31_svc_kernel_rbf.log",
    # """
    # Par document
    #             Window Size - 1gram
    #             OneVsRest kernel SVC rbf (Radial Basis Function)
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=ovr_svc_kernel_rbf_1gram_pipeline),

    Tester("../results/logs/english/31_rf.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Random Forest
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_rf_1gram_pipeline,
           force=force or False),

    # Tester("../results/logs/english/31_nn.log",
    #        """
    #             Par document
    #             Window Size - 1gram
    #             NearestNeighbors
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=ovr_nn_1gram_pipeline),

    # Tester("../results/logs/english/31_occ_l_svc.log",
    #        """
    #             Par document
    #             Window Size - 1gram
    #             OutputCodeClassifier
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=occ_l_svc_pipeline),

    Tester("../results/logs/english/31_dt.log",
           """
                Par document
                Window Size - 1gram
                Decision Tree
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_dt_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_g_NB.log",
           """
               Par document
               Window Size - 1gram
               OneVsRest Gaussian Naive Bayes
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_g_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_m_NB.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_m_NB__lemmatizer_filtered_by_postag.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_m_NB__lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_m_NB__lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (bests tags only)
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_m_NB__2alpha_lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document 2 alphas codes (bests tags only)
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag 2
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, only_2alpha_tag_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_b_NB.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline,
           force=force or False),


    Tester("../results/logs/english/32_b_NB__lemmatizer_filtered_by_postag.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
                Lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter, default_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_b_NB__lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
                Lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_b_NB__lemmatizer_filtered_by_postag_2_bests_only.log",
           """
                Par document (bests_tags_only)
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
                Lemmatizer filtered by postag
           """,
           document_adapters=[lemmatizer_filtered_corpus_adapter_2, default_corpus_adapter, bests_tag_filter_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline,
           force=force or False),

    Tester("../results/logs/english/32_ab.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest AdaBoost
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_ab_pipeline,
           force=force or False),


    Tester("../results/logs/english/41_dt_count_vectorizer.log",
           """
                Par document
                CountVectorizer
                Decision Tree
           """,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_dt_count_vectorizer_pipeline,
           force=force or False),
]
""":type: list[Tester]"""

# Run all needed tests
for tester in testers:
    tester.test()

# Save the json file list for the logs_viewer
with open("../results/logs_viewer/english_logs.json", 'w') as f:
    f.write('[\n\t"')
    f.write('",\n\t"'.join(
        '../' + t.filename + ".json" for t in testers
    ))
    f.write('"\n]')