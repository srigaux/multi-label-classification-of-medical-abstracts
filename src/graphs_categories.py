# coding=utf-8
from collections import Counter
from numpy.core.multiarray import arange
from src.classifier_3cgp.importers.joblib_importer import JoblibImporter
import matplotlib.pyplot as plt
from src.classifier_3cgp.utils.flat_colors import FlatColors

__author__ = 'Sébastien Rigaux'

import_dir = "../data/documents/"

french_doc = JoblibImporter(import_dir + "french.bin").import_corpus()

tags = french_doc.tags()
flat_tags = [item for sublist in tags for item in sublist]
group_tags = Counter(flat_tags).most_common()
keys = [key for key, value in group_tags]
values = [value for key, value in group_tags]

x = arange(1, len(group_tags) + 1)

fig = plt.Figure()

sp_fr = plt.subplot(211)
sp_fr.set_xlabel('CNGEfr')

threshold = 10

first_under_5 = (i for i,v in enumerate(values) if v < threshold).next()
line = plt.plot((0, len(group_tags) + 1), (threshold, threshold), 'k-', color=FlatColors.to_dec(FlatColors.wet_asphalt))
line = plt.plot((first_under_5, first_under_5), (0, 200), 'k-', color=FlatColors.to_dec(FlatColors.concrete))

bar = plt.bar(x, values, color=FlatColors.to_dec(FlatColors.peter_river), edgecolor = "none")

sp_fr.text(first_under_5 - 1.5, 205, r'%d%%' % (float(first_under_5) / len(values) * 100), fontsize=8, color=FlatColors.to_dec(FlatColors.concrete))

plt.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    labelsize=6,
    gridOn='true')        # labels along the bottom edge are off

plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

plt.ylim((0,200))


english_doc = JoblibImporter(import_dir + "english.bin").import_corpus()

tags = english_doc.tags()
flat_tags = [item for sublist in tags for item in sublist]
group_tags = Counter(flat_tags).most_common()
keys = [key for key, value in group_tags]
values = [value for key, value in group_tags]

x = arange(1, len(group_tags) + 1)

sp_en = plt.subplot(212)
sp_en.set_xlabel('WONCAen')

first_under_5 = (i for i,v in enumerate(values) if v < threshold).next()
line = plt.plot((0, len(group_tags) + 1), (threshold, threshold), 'k-', color=FlatColors.to_dec(FlatColors.wet_asphalt))
line = plt.plot((first_under_5, first_under_5), (0, 200), 'k-', color=FlatColors.to_dec(FlatColors.concrete))

bar = plt.bar(x, values, color=FlatColors.to_dec(FlatColors.alizarin), edgecolor = "none")

sp_en.text(first_under_5 - 1.5, 205, r'%d%%' % (float(first_under_5) / len(values) * 100), fontsize=8, color=FlatColors.to_dec(FlatColors.concrete))

plt.ylim((0,200))

plt.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    labelsize=6,
    gridOn='true')        # labels along the bottom edge are off

plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off

# plt.show()
plt.savefig('../results/graphs/categories.pdf')