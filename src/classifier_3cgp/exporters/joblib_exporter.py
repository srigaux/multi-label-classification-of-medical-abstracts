# coding=utf-8
import os
from sklearn.externals import joblib
import sys
from src.classifier_3cgp.exporters.exporter import Exporter
from src.classifier_3cgp.models.corpus import Corpus

__author__ = 'Sébastien Rigaux'


class JoblibExporter(Exporter):
    """
    Corpus exporter using JobLib library
    """

    def __init__(self, overwrite=False):
        """

        :param overwrite: ``True`` to overwrite files if it already exists, ``False`` otherwise
        :type overwrite: bool
        """
        super(JoblibExporter, self).__init__()
        self.overwrite = overwrite

    def export(self, corpus_factory, file_path, overwrite=False, verbose=True):
        """
        Export a document to a file if it does'nt already exists
         or if ``self.overwrite == True`` or if ``overwrite == True``

        :param corpus_factory: A delegate call to get the corpus to export if necessary
        :type corpus_factory: () -> Corpus

        :param file_path: The file path of the file to save
        :type file_path: str
        """

        if verbose:
            sys.stdout.write("Exporting %s ... " % file_path)

        if not self.overwrite and not overwrite and os.path.isfile(file_path):

            if verbose:
                sys.stdout.write("Skipped : Existing file\n")

            return

        joblib.dump(corpus_factory(), file_path)

        if verbose:
            sys.stdout.write("OK\n")
