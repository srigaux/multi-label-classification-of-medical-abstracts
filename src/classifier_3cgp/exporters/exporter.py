# coding=utf-8
from abc import abstractmethod
from src.classifier_3cgp.models.corpus import Corpus

__author__ = 'Sébastien Rigaux'


class Exporter(object):
    """
    Abstract class which allows to export a ``Corpus``
    """

    def __init__(self):
        super(Exporter, self).__init__()

    @abstractmethod
    def export(self, corpus_delegate, file_path):
        """
        :param corpus: The delegate which returns the corpus to export
        :type corpus: () -> Corpus

        :param file_path: The filePath where to export the corpus
        :type file_path: str
        """
        pass

