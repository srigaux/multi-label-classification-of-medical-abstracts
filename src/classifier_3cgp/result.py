# coding=utf-8
from sklearn.base import BaseEstimator

__author__ = 'Sébastien Rigaux'


class Result(object):
    """
    Helper class to easily get results properties from a dict result
    """

    lang = "lang"
    lang_fr = "fr"
    lang_en = "en"

    doc = "doc"
    doc_lemmatized = "lemmatized"
    doc_lemmatized_filtered = "lemmatized filtered"
    doc_stemmed = "stemmed"
    doc_original = ""

    vect = "vect"
    vect_presence = "binnary"
    vect_occurrence = "count"
    vect_tf = "Tf"
    vect_tfidf = "Tf-Idf"

    fs = "fs"
    fs_bns = "bnf"
    fs_chi2 = "chi2"
    fs_mi = "mi_2"
    fs_mi3 = "mi3_2"
    fs_truncatedSVD = "TruncatedSVD"
    fs_none = "none"

    fs_k = "fs__k"
    fs_n_components = "fs__n_components"
    fs_features_count = "fs_nb_features"

    clf = "clf"
    clf_adaBoost = "AdaBoost"
    clf_bernoulliNB = "BernoulliNB"
    clf_dummy_mostFrequent = "Dummy-MostFrequent"
    clf_dummy_stratified = "Dummy-Stratified"
    clf_kNeighborsClassifier = "KNeighborsClassifier"
    clf_multinomialNB = "MultinomialNB"
    clf_passiveAggressive = "PassiveAggressive"
    clf_perceptron = "Perceptron"
    clf_ridge = "Ridge"
    clf_sgd = "SGD"
    clf_linear_svm = "svc"

    clf_c = "clf__estimator__C"
    clf_alpha = "clf__estimator__alpha"

    f1_mean = "f1 mean"

    others_n_samples = "n_samples"
    others_class_weight = "clf__estimator__class_weight"
    others_leaf_size = "clf__estimator__leaf_size"
    others_penalty = "clf__estimator__penalty"
    others_dual = "clf__estimator__dual"
    others_norm = "vect__norm"
    others_learning_rate = "clf__estimator__learning_rate"
    others_n_neighbors = "clf__estimator__n_neighbors"
    others_f1_std = "f1 std"
    others_max_df = "vect__max_df"
    others_tol = "clf__estimator__tol"
    others_time = "cross_val_time"
    others_eta0 = "clf__estimator__eta0"
    others_stop_words = "vect__stop_words"
    others_max_features = "vect__max_features"
    others_hash = "hash"
    others_p = "clf__estimator__p"
    others_sublinear_tf = "vect__sublinear_tf"
    others_min_df = "vect__min_df"
    others_loss = "clf__estimator__loss"
    others_ngram_range = "vect__ngram_range"
    others_n_estimators = "clf__estimator__n_estimators"
    others_weights = "clf__estimator__weights"

    __floats = [clf_c, clf_alpha, f1_mean, others_f1_std]
    """ The floats properties """

    __ints = [others_n_samples, fs_k]
    """ The ints properties """

    def __init__(self, *initial_data, **kwargs):
        super(Result, self).__init__()

        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])

        for key in kwargs:
            setattr(self, key, kwargs[key])

    @property
    def fs_nb_features(self):
        """
        :return: The number of features selected for the FI
                   (``k`` for FS methods and ``n_components`` for FE methods)

        :rtype: int
        """

        nb_f = getattr(self, self.fs_k)

        if nb_f == '':
            nb_f = getattr(self, self.fs_n_components)

        if nb_f == '':
            nb_f = -1

        return int(nb_f)

    def __setattr__(self, name, value):

        if value != '' and value is not None:
            if name in Result.__floats:
                value = float(value)

            if name in Result.__ints:
                value = int(value)

        return super(Result, self).__setattr__(name, value)

    def __repr__(self):
        return "l:{lang:<2} d:{doc:<19} v:{vect:<7} fs:{fs:12} " \
               "k:{fs__k:5} {fs__n_components:5} " \
               "C:{clf__estimator__C:5} a:{clf__estimator__alpha:5} f1:{f1 mean:5}".format(**self.__dict__)


