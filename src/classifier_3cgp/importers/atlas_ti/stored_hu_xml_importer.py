# coding=utf-8
import xml.etree.ElementTree as etree
from xml.etree import ElementTree
from objects import *
from src.classifier_3cgp.importers.atlas_ti.adapters.stored_hu_corpus_adapter import PerDocumentStoredHUCorpusAdapter
from src.classifier_3cgp.importers.importer import Importer

__author__ = 'Sébastien Rigaux'


class StoredHUXmlImporter(Importer):
    """
    StoredHUXml importer
    """

    def __init__(self, file_path, adapter=None):
        """
        :param file_path: The path of the file to import
        :type file_path: str

        :param adapter: A HU document adapter (default: `PerDocumentStoredHUCorpusAdapter`)
        :type adapter: StoredHUCorpusAdapter
        """
        super(StoredHUXmlImporter, self).__init__()

        Importer._assert_file(file_path)

        self._file_path = file_path
        self._adapter = adapter or PerDocumentStoredHUCorpusAdapter()
        self._stored_HU = None
        """:type: StoredHU"""

    @property
    def adapter(self):
        """
        Get the HU document adapter

        :return: The HU document adapter
        :rtype: StoredHUCorpusAdapter
        """
        return self._adapter

    @adapter.setter
    def adapter(self, value):
        """
        Set the HU document adapter
        :param value: The HU document adapter to set
        """
        if self._adapter == value:
            return

        self._adapter = value
        self._imported_document = None

    @property
    def filename(self):
        """
        Get the path of the file to import

        :return: The path of the file to import
        :rtype: str
        """
        return self._file_path

    def _import_corpus(self):

        if self._stored_HU is None:
            self.__parse()

        corpus = self.adapter.to_corpus(self._stored_HU)

        return corpus

    def __parse(self):

        print "Parsing Xml file ", self._file_path

        stored_hu = StoredHU()

        tree = etree.parse(self._file_path)
        root = tree.getroot()

        print "Parsing documents ..."

        xml_documents = root.find('primDocs')
        assert xml_documents is not None, "The xml file has no 'primDocs' node."

        for xml_document in xml_documents.findall('primDoc'):
            document = Document(id=xml_document.get('id'),
                                name=xml_document.get("name"))

            stored_hu.add_document(document)

            for xml_quotation in xml_document.find('quotations'):
                quotation = Quotation(id=xml_quotation.get('id'))

                quotation.append_content(xml_quotation.text)

                for xml_content in xml_quotation.find('content'):
                    content = ElementTree.tostring(xml_content)
                    quotation.append_content(content)

                stored_hu.add_quotation(quotation, document)

        print "Parsing codes ..."

        xml_codes = root.find('codes')
        assert xml_codes is not None, "The xml file has no 'codes' node."

        for xml_code in xml_codes.findall('code'):
            code = Code(id=xml_code.get('id'),
                        name=xml_code.get('name'))

            stored_hu.add_code(code)

        print "Parsing relationships"
        print "\t Linking codes to families ..."

        xml_code_families = root.find('families/codeFamilies')
        assert xml_code_families is not None, "The xml file has no 'families/codeFamilies' node."

        for xml_code_family in xml_code_families.findall('codeFamily'):
            code_family = CodeFamily(id=xml_code_family.get('id'),
                                     name=xml_code_family.get('name'))

            stored_hu.add_code_family(code_family)

            for xml_code_family_item in xml_code_family.findall('item'):
                code_family.add_code_with_id(xml_code_family_item.get('id'))

        print "\t Linking quotations to codes ..."

        xml_codings = root.find('links/objectSegmentLinks/codings')
        assert xml_codings is not None, "The xml file has no 'links/objectSegmentLinks/codings' node."

        for xml_link in xml_codings.findall('iLink'):
            stored_hu.add_coding(code_id=xml_link.get('obj'),
                                 quotation_id=xml_link.get('qRef'))

        self._stored_HU = stored_hu

