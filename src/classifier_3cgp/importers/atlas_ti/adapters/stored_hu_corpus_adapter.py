# coding=utf-8
from abc import abstractmethod

__author__ = 'Sébastien Rigaux'
from ..objects import Document as StoredHUDocument, StoredHU, Quotation
from ....models.corpus import Corpus
from ....models.document import Document


class StoredHUCorpusAdapter(object):
    """
    Adapts a ``StoredHU`` to a ``Corpus``
    """

    def __init__(self):
        super(StoredHUCorpusAdapter, self).__init__()

    @abstractmethod
    def to_corpus(self, stored_hu):
        """
        Adapts a ``StoredHU`` to a ``Corpus``

        :param stored_hu: The ``StoredHU`` to adapt
        :type stored_hu: StoredHU

        :return: A ``Corpus``
        :rtype : Corpus
        """
        pass


class PerDocumentStoredHUCorpusAdapter(StoredHUCorpusAdapter):
    """
    Adapts a ``StoredHU`` to a ``Corpus``
    """

    def __init__(self):
        super(PerDocumentStoredHUCorpusAdapter, self).__init__()

    def to_document(self, document):
        """
        Adapts a ``StoredHUDocument`` to a ``Document`` (One document per abstract)

        :param document: The ``StoredHUDocument`` to adapt
        :type document: StoredHUDocument

        :return: A ``Document``
        :rtype: Document
        """

        codes = list(c.name for c in document.codes)

        document = Document(text=document.text,
                            tags=codes)

        return document

    def to_corpus(self, stored_hu):
        documents = list(self.to_document(d) for d in stored_hu.documents.values())
        corpus = Corpus(documents=documents)

        return corpus


class PerQuotationStoredHUCorpusAdapter(StoredHUCorpusAdapter):
    """
    Adapts a ``StoredHU`` to a ``Corpus`` (One document per quotation)
    """

    def __init__(self):
        super(PerQuotationStoredHUCorpusAdapter, self).__init__()

    @staticmethod
    def to_document(quotation):
        """
        Adapts a ``Quotation`` to a ``Document``

        :param quotation: The ``Quotation`` to adapt
        :type quotation: Quotation

        :return: A ``Document``
        :rtype: Document
        """

        codes = list(c.name for c in quotation.codes.values())
        document = Document(text=quotation.content,
                            tags=codes)

        return document

    def to_corpus(self, stored_hu):

        documents = list(
            self.to_document(q)
            for d in stored_hu.documents.values()
            for q in d.quotations.values())

        corpus = Corpus(documents=documents)

        return corpus


class OnlyCodedQuotationPerDocumentHUDocumentAdapter(PerDocumentStoredHUCorpusAdapter):
    """
    Adapts a ``StoredHU`` to a ``Corpus`` (One document per annotated quotation only)
    """

    def __init__(self):
        super(OnlyCodedQuotationPerDocumentHUDocumentAdapter, self).__init__()

    def to_document(self, document):

        codes = list(c.name for c in document.codes)

        text = ''.join((q.content for q in document.quotations.values() if len(q.codes)))

        pair = Document(text=text,
                        tags=codes)

        return pair
