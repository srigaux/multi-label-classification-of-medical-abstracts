# coding=utf-8
from collections import OrderedDict

__author__ = 'Sébastien Rigaux'


class StoredHUItem:
    """
    Base class for Items
    """
    def __init__(self, id):
        """
        :param id: The id of the item
        :type id: str
        """

        assert isinstance(id, str)

        self.id = id
        self.storedHU = None


class Code(StoredHUItem):
    """
    Represents a tag
    """
    def __init__(self, id, name):
        """
        :param id: The code's id
        :type id: str

        :param name: The code's name
        :type name: str
        """

        StoredHUItem.__init__(self, id)

        self.name = name

        self.family = None
        """:type: CodeFamily"""


class CodeFamily(StoredHUItem):
    """
    Represents a group of tags
    """

    def __init__(self, id, name):
        """
        :param id: The family's id
        :type id: str

        :param name: The family's name
        :type name: str
        """

        StoredHUItem.__init__(self, id)
        self.name = name
        self.codes = []
        """:type: list[Code]"""

    def add_code(self, code):
        """
        Add a code to the family

        :param code: The code to add
        :type code: Code
        """

        assert isinstance(code, Code)

        code.family = self
        self.codes.append(code)

    def add_code_with_id(self, code_id):
        """
        Add the code with id `code_id`

        :param code_id: The id of the code to add in family
        :type code_id: str
        """
        assert isinstance(code_id, str)
        self.add_code(self.storedHU.items[code_id])


class Quotation(StoredHUItem):
    """
    Represents a paragraph/quotation/sentence of an abstract
    """

    def __init__(self, id):
        """
        :param id: The quotation's id
        :type id: str
        """
        StoredHUItem.__init__(self, id)
        self.content = ''
        self.codes = {}
        """:type: dict[str,Code]"""

    def append_content(self, content):
        """
        Add content to the quotation

        :param content: The content to add
        :type content:str
        """
        assert isinstance(content, str)
        self.content += content

    def add_code(self, code):
        """
        Add a code to the quotation

        :param code: The code to add
        :type code: Code
        """

        assert isinstance(code, Code)

        self.codes[code.name] = code


class Document(StoredHUItem):
    """
    Represents an abstract
    """

    def __init__(self, id, name):
        """
        :param id: The document's id
        :type id: str

        :param name: The document's name
        :type name: str
        """
        StoredHUItem.__init__(self, id)
        self.name = name
        self.quotations = OrderedDict()
        """:type: OrderedDict[str,Quotation]"""

    def add_quotation(self, quotation):
        """
        Add a quotation to the document

        :param quotation: The quotation to add
        :type quotation: Quotation
        """
        assert isinstance(quotation, Quotation)
        self.quotations[quotation.id] = quotation

    @property
    def text(self):
        """
        :return: The document's text
        :rtype: str
        """
        return ''.join((q.content for q in self.quotations.values()))

    @property
    def codes(self):
        """
        :return: The document's codes
        :rtype: list[Code]
        """
        return [c for q in self.quotations.values() for c in q.codes.values()]


class StoredHU:
    def __init__(self):
        self.items = {}
        """:type: dict[str,StoredHUItem]"""

        self.documents = OrderedDict()
        """:type: OrderedDict[str,Document]"""

        self.codes = OrderedDict()
        """:type: OrderedDict[str,Code]"""

        self.codeFamilies = OrderedDict()
        """:type: OrderedDict[str,CodeFamily]"""

    def __add(self, item):
        """
        Add an item

        :param item: The item to add
        :type item: StoredHUItem
        """

        assert isinstance(item, StoredHUItem)

        item.storedHU = self
        self.items[item.id] = item

    def add_document(self, document):
        """
        Add a document

        :param document: The document to add
        :type document: Document
        """

        assert isinstance(document, Document)

        self.__add(document)
        self.documents[document.id] = document

    def add_quotation(self, quotation, in_document):
        """
        Add a quotation in the document ``in_document``

        :param quotation: The quotation to add
        :type quotation: Quotation

        :param in_document: The document which to add the quotation
        :type in_document: Document
        """

        assert isinstance(quotation, Quotation)
        assert isinstance(in_document, Document)

        self.__add(quotation)
        in_document.add_quotation(quotation)

    def add_code(self, code):
        """
        Add a code

        :param code: The code to add
        :type code: Code
        """
        assert isinstance(code, Code)

        self.__add(code)
        self.codes[code.name] = code

    def add_code_family(self, code_family):
        """
        Add a code family

        :param code_family: The family to add
        :type code_family: CodeFamily
        """

        assert isinstance(code_family, CodeFamily)

        self.__add(code_family)
        self.codeFamilies[code_family.name] = code_family

    def add_coding(self, code_id, quotation_id):
        """
        Add a relationship between a code and a quotation

        :param code_id: The code's id
        :type code_id: str

        :param quotation_id: The quotation's id
        :type quotation_id: str

        :return: The code and the quotation
        :rtype: (Code, Quotation)
        """
        code = self.items[code_id]
        quotation = self.items[quotation_id]

        assert code is not None, 'No code with id : ' + code_id
        assert quotation is not None, 'No quotation with id : ' + quotation_id

        quotation.add_code(code)

        return code, quotation
