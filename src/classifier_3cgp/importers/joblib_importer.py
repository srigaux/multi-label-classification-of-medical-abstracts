# coding=utf-8
from sklearn.externals import joblib

from src.classifier_3cgp.importers.importer import Importer

__author__ = 'Sébastien Rigaux'


class JoblibImporter(Importer):
    """
    Corpus importer using JobLib library
    """

    def __init__(self, file_path):
        """
        :param file_path: The path of the file to import
        :type file_path: str
        """
        super(JoblibImporter, self).__init__()

        Importer._assert_file(file_path)
        self._file_path = file_path

    def _import_corpus(self):
        return joblib.load(self._file_path)