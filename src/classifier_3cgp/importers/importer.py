# coding=utf-8
from abc import abstractmethod
import os
from src.classifier_3cgp.models.corpus import Corpus

__author__ = 'Sébastien Rigaux'


class Importer(object):
    """
    Corpus importer
    """

    def __init__(self):
        super(Importer, self).__init__()

        self._imported_document = None

    def import_corpus(self):
        """
        Imports a corpus

        :return: The imported corpus
        :rtype: Corpus
        """

        if self._imported_document is not None:
            return self._imported_document

        self._imported_document = self._import_corpus()

        return self._imported_document

    @abstractmethod
    def _import_corpus(self):
        """
        Actually imports the corpus

        :return: The imported corpus
        :rtype: Corpus
        """
        pass

    @staticmethod
    def _assert_file(file_path):
        """
        Assert that a file exists at ``file_path``

        :param file_path: The filepath to check
        :type file_path: str
        """
        assert file_path, "FilePath required"
        assert os.path.isfile(file_path), "File '%s' does not exists." % file_path