# coding=utf-8
import os
import xml.etree.ElementTree as etree
from os import listdir

from src.classifier_3cgp.importers.wonca.objects import Abstract

__author__ = 'Sébastien Rigaux'


class WoncaArticleParser:
    """
    Wonca data articles parser (XML files)
    """

    def __init__(self):
        pass

    @staticmethod
    def parse(filename, fix_multiple_roots_xml=True):
        """
        Parse an XML file

        :param filename: The path of the file to parse
        :type filename: str

        :param fix_multiple_roots_xml: If ``True`` add an XmlNode before parsing
            (for invalids Xml files)
        :type fix_multiple_roots_xml: bool

        :return: The abstract parsed
        :rtype: Abstract
        """

        assert filename is not None

        print "Parsing Xml file ", filename

        if fix_multiple_roots_xml:
            lines = WoncaArticleParser.__fix_multiple_roots_xml(filename)
            xml = etree.fromstringlist(lines)
        else:
            xml = etree.parse(filename)

        abstract = Abstract(
            numero=int(xml.findtext('numero')),
            auteur=xml.findtext('auteur'),
            organisation=xml.findtext('organisation'),
            co_auteurs=xml.findtext('co_Auteurs'),
            topic=xml.findtext('topic'),
            titre=xml.findtext('titre'),
            article=xml.findtext('article')
        )

        return abstract

    @staticmethod
    def parser_folder(path, fix_multiple_roots_xml=True):
        """
        Parse a folder containing XML files

        :param path: The path to the directory
        :type path: str

        :return: A dictionary of abstracts identified by their number
        :rtype: dict[int, Abstract]
        """
        return dict((
            (a.numero, a)
            for a in (
            WoncaArticleParser.parse(filename=os.path.join(path, f),
                                     fix_multiple_roots_xml=fix_multiple_roots_xml)
            for f in listdir(path) if f.endswith('.xml'))
        ))

    @staticmethod
    def __fix_multiple_roots_xml(filename):
        """
        :param filename: The path of the file to parse
        :type filename: str

        :return: The file lines including the new XmlRootNode (``<abstract />``)
        :rtype: list[str]
        """
        f = open(filename, 'r')
        lines = f.readlines()
        lines.insert(1, "<abstract>")
        lines.append("</abstract>")

        return lines
