# coding=utf-8
from src.classifier_3cgp.importers.importer import Importer
from src.classifier_3cgp.importers.wonca.objects import WoncaData
from src.classifier_3cgp.importers.wonca.wonca_annotation_parser import WoncaAnnotationsParser
from src.classifier_3cgp.importers.wonca.wonca_article_parser import WoncaArticleParser
from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.models.document import Document

__author__ = 'Sébastien Rigaux'


class WoncaImporter(Importer):
    """
    Wonca data importer

    :Example:

    >>> path = '../../../../data/wonca/paris_2007/'
    >>> doc = WoncaImporter(annotations_csv_filename=path + 'annotations.csv',
    >>> articles_xml_directory_path=path + 'xml_abstract').import_corpus()

    >>> for p in doc.documents:
    >>>    print "%-40s\t%20s\t%4d" % (p.text[:40], ", ".join(p.tags)[:20], len(p.text))

    >>> print "Total : ", len(doc.documents)
    >>> print "Avg CharCount : ", reduce(lambda sum, p: sum + len(p.text), doc.documents, 0) / len(doc.documents)
    >>> print "Avg TagsCount : ", reduce(lambda sum, p: sum + len(p.tags), doc.documents, 0) / len(doc.documents)
    """

    def __init__(self,
                 annotations_csv_filename, articles_xml_directory_path,
                 annotations_parser=None, abstracts_parser=None):
        """
        :param annotations_csv_filename: The path to the annotations's CSV file
        :type annotations_csv_filename: str

        :param articles_xml_directory_path: The path to the abstracts's XML files
        :type articles_xml_directory_path: str

        :param annotations_parser: The annotations's XML file parser
        :type annotations_parser: WoncaAnnotationsParser

        :param abstracts_parser: The abstracts's CSV file parser
        :type abstracts_parser: WoncaArticleParser
        """

        super(WoncaImporter, self).__init__()

        self._annotations_csv_filename = annotations_csv_filename
        self._articles_xml_directory_path = articles_xml_directory_path
        self._annotations_parser = annotations_parser or WoncaAnnotationsParser()
        self._articles_parser = abstracts_parser or WoncaArticleParser()

    def __parse(self):
        """
        :return: The Wonca data
        :rtype: WoncaData
        """
        articles = self._articles_parser.parser_folder(self._articles_xml_directory_path)
        annotations = self._annotations_parser.parse(self._annotations_csv_filename)

        return WoncaData(articles=articles,
                         annotations=annotations)

    def _import_corpus(self):
        wonca_data = self.__parse()

        documents = [Document(text=i.abstract.titre + " " + i.abstract.article,
                          tags=i.annotations.tags)
                 for num, i in wonca_data]

        return Corpus(documents)
