# coding=utf-8

__author__ = 'Sébastien Rigaux'


class Abstract:
    """
    An abstract
    """

    def __init__(self, numero, article, titre="", topic="", auteur="", organisation="", co_auteurs=""):
        """
        :param numero: the abstract's number
        :type numero: int

        :param article: the abstract's article
        :type article: str

        :param titre: the abstract's title
        :type titre: str

        :param topic: the abstract's topic
        :type topic: str

        :param auteur: the abstract's author
        :type auteur: str

        :param organisation: the abstract's organisation
        :type organisation: str

        :param co_auteurs: the abstract's coauthors
        :type co_auteurs: str
        """
        self.numero = numero
        self.auteur = auteur
        self.organisation = organisation
        self.co_auteurs = co_auteurs
        self.titre = titre
        self.topic = topic
        self.article = article


class Annotations:
    """
    A tag list
    """

    def __init__(self, numero, tags, tags_delimiter=';'):
        """

        :param numero: The abstract's number
        :type numero:int

        :param tags: A list of tags, or a delimited string of tags
        :type tags: list[str] | str

        :param tags_delimiter: the delimiter used for ``tags`` or ``None``
        :type tags_delimiter: str
        """
        self.numero = numero

        if (isinstance(tags, list)):
            self.tags = tags
        else:
            self.tags = self.__extract_tags(tags, tags_delimiter)

    @staticmethod
    def __extract_tags(tags_str, delimiter=';'):
        """
        :param tags_str: The delimited string of tags
        :type tags_str: str

        :param delimiter: The delimiter
        :type delimiter: str

        :return: The tags
        :rtype: list[str]
        """
        raw_tags = tags_str.split(delimiter)

        return list(t.strip().upper() for t in raw_tags)


class WoncaDataItem:
    """
    An item of the wonca corpus (Document)
    """

    def __init__(self, abstract, annotations):
        """

        :param abstract: The abstract
        :type abstract: Abstract

        :param annotations: The annotations
        :type annotations: Annotations
        """
        self.abstract = abstract
        self.annotations = annotations


class WoncaData:
    """
    The wonca data (Corpus)
    """

    def __init__(self, articles=None, annotations=None):
        """
        :param articles: The articles
        :type articles: dict[int, Article]

        :param annotations: The annotations
        :type annotations: dict[int, Annotations]
        """
        self.articles = articles or dict()
        self.annotations = annotations or dict()

        self._data_items = dict([
                                    (n, WoncaDataItem(abstract=self.articles[n],
                                                      annotations=self.annotations[n]))
                                    for n in self.articles.keys() if n in self.annotations
                                    ])
        """:type : dict[int,WoncaDataItem]"""

    @property
    def data_items(self):
        """
        Returns the items

        :return: The items
        :rtype: list[WoncaDataItem]
        """
        return self._data_items.values()

    def __iter__(self):
        """
        :rtype: __genrator[int,WoncaDataItem]
        """
        return (item for item in self._data_items.items())

    def __add_abstract(self, abstract):
        """
        Add an abstract

        :param abstract: The abstract to add
        :type abstract: Abstract
        """
        n = abstract.numero
        self.articles[n] = abstract

        if self.annotations.has_key(n):
            self._data_items[n] = \
                WoncaDataItem(abstract=abstract,
                              annotations=self.annotations[n])

    def __add_annotations(self, annotations):
        """
        Add an annotation

        :param annotations: The annotations to add
        :type annotations: Annotations
        """
        n = annotations.numero
        self.annotations[n] = annotations

        if n in self.articles:
            self._data_items[n] = \
                WoncaDataItem(abstract=self.articles[n],
                              annotations=annotations)
