# coding=utf-8
import csv
from src.classifier_3cgp.importers.wonca.objects import Annotations

__author__ = 'Sébastien Rigaux'


class WoncaAnnotationsParser:
    """
    Wonca data annotations parser (CSV)
    """

    def __init__(self):
        pass

    @staticmethod
    def parse(filename, csv_delimiter=';', tags_delimiter=';'):
        """
        Parse a csv file

        :param filename: The path of the file to import
        :type filename: str

        :param csv_delimiter: The delimiter used in the CSV file (';')
        :type csv_delimiter:str

        :param tags_delimiter: The delimiter used to join the tags (';')
        :type tags_delimiter:str

        :return A dictionary of annotations identified by their number
        :rtype dict[int, Annotations]
        """

        assert filename is not None

        print "Parsing Csv file ", filename

        with open(filename, 'rU') as f:
            reader = csv.reader(f, delimiter=csv_delimiter)

            return dict(
                (a.numero, a)
                for a in (
                    Annotations(numero=int(numero),
                                tags=tags.decode('utf-8'),
                                tags_delimiter=tags_delimiter
                                )
                    for numero, tags in reader
                )
            )