# coding=utf-8
from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.models.document import Document

__author__ = 'Sébastien Rigaux'


class CorpusAdapter(object):
    """
    Base class to adapt a corpus
    """

    def __init__(self):
        pass

    def adapt_text(self, text):
        """
        Adapts the text of a document

        :param text: The text to adapt
        :type text: str

        :return: The adapted text
        :rtype: str
        """
        return text

    def adapt_tag(self, tag):
        """
        Adapts a tag of a document

        :param tag: The tag to adapt
        :type tag: str

        :return: The adapted text
        :rtype: str
        """
        return tag

    def adapt_tags(self, tags):
        """
        Adapts the tags of a document

        :param tags: The tags to adapt
        :type tags: list[str]

        :return: The adapted tags
        :rtype: list[str]
        """
        new_tags = [at
                    for at in (self.adapt_tag(t)
                               for t in tags)
                    if at is not None]

        return new_tags

    def adapt_document(self, document):
        """
        Adapts a document

        :param document: The document to adapt
        :type document: Document

        :return: The adapted document
        :rtype: Document
        """

        text = self.adapt_text(document.text)
        if text is None:
            return None

        tags = self.adapt_tags(document.tags)

        new_document = Document(text, tags)

        return new_document

    def adapt_documents(self, documents):
        """
        Adapts the documents of a corpus

        :param documents: The documents to adapt
        :type documents: list[Document]

        :return: The adapted documents
        :rtype: list[Document]
        """
        new_pairs = [self.adapt_document(p) for p in documents]

        return new_pairs

    def adapt_corpus(self, corpus):
        """
        Adapts a corpus

        :param corpus: The corpus to adapt
        :type corpus: Corpus

        :return: The adapted corpus
        :rtype: Corpus
        """

        new_documents = self.adapt_documents(corpus.documents)
        new_corpus = Corpus(new_documents)

        return new_corpus
