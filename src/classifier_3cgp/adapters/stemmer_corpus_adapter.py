# coding=utf-8

from nltk import RegexpTokenizer
from nltk.stem.snowball import FrenchStemmer
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'

class FrenchStemmerCorpusAdapter(CorpusAdapter):
    """
    A corpus adapter for french stemming
    (French Snowball stemmer)
    """

    def __init__(self):
        CorpusAdapter.__init__(self)

        # http://www.fabienpoulard.info/post/2008/03/05/Tokenisation-en-mots-avec-NLTK

        reg_words = r'''(?x)
                             \d+(\.\d+)?\s*%   # les pourcentages
                           | 's                # l'appartenance anglaise 's
                           | \w'               # les contractions d', l', j', t', s'
                        '''
        reg_words += u"| \w\u2019"    # unicode version
        reg_words += u"|\w+|[^\w\s]"  # antislashs

        self.tokenizer = RegexpTokenizer(reg_words)
        self.stemmer = FrenchStemmer()

    def adapt_text(self, text):

        tokens = self.tokenizer.tokenize(text)
        new_text = ' '.join(
            (self.stemmer.stem(t) for t in tokens)
        )

        return new_text


class EnglishStemmerCorpusAdapter(CorpusAdapter):
    """
    A corpus adapter for english stemming
    (Porter stemmer)
    """

    def __init__(self):
        CorpusAdapter.__init__(self)

        self.stemmer = PorterStemmer()

    def adapt_text(self, text):

        tokens = word_tokenize(text)
        new_text = ' '.join(
            (self.stemmer.stem(t) for t in tokens)
        )

        return new_text