# coding=utf-8
from ..models.document import Document
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'


class DocumentFilterCorpusAdapter(CorpusAdapter):
    """
    Corpus adapter which allows to filter the documents of the corpus
    """

    def __init__(self, filter=None):
        """

        :param filter: The document filter predicate
        :type filter: (Document) -> bool
        """
        CorpusAdapter.__init__(self)

        self.filter = lambda s: True

        if filter is not None:
            assert isinstance(filter, type(self.filter))
            self.filter = filter

    def adapt_documents(self, documents):

        new_pairs = list(p
                         for p in documents
                         if self.filter(p.text))

        return new_pairs