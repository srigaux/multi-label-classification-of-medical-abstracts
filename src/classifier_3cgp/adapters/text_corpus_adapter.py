# coding=utf-8
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter


__author__ = 'Sébastien Rigaux'


class TextCorpusAdapter(CorpusAdapter):
    """
    A corpus adapter which allows to adapt the texts of a corpus
    """
    def __init__(self, adapter):
        """

        :param adapter: The text adapter delegate
        :type adapter: (str) -> str
        """
        CorpusAdapter.__init__(self)

        self.adapter = lambda s: s

        if adapter is not None:
            assert isinstance(adapter, type(self.adapter))
            self.adapter = adapter

    def adapt_text(self, text):

        return self.adapter(text)