# coding=utf-8
from bs4 import BeautifulSoup

from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'


class TagTextAdapterFilterCorpusAdapter(CorpusAdapter):
    """
    Corpus adapter which allows to adapt/filter the tags/texts of a corpus
    """

    def __init__(self, tag_adapter=None, tag_filter=None, text_adapter=None, text_filter=None):
        """

        :param tag_adapter: A tag adapter delegate (or ``None``)
        :type tag_adapter: (str) -> str

        :param tag_filter: A tag filter predicate (or ``None``)
        :type tag_filter: (str) -> bool

        :param text_adapter: A text adapter delegate (or ``None``)
        :type text_adapter: (str) -> str

        :param text_filter: A text filter predicate (or ``None``)
        :type text_filter: (str) -> bool
        """

        CorpusAdapter.__init__(self)

        self.tag_adapter = lambda s: s[:s.find(' ')]
        self.tag_filter = lambda s: True
        self.text_adapter = lambda s: BeautifulSoup(s).get_text()
        self.text_filter = lambda s: True

        if tag_adapter is not None:
            assert isinstance(tag_adapter, type(self.tag_adapter))
            self.tag_adapter = tag_adapter

        if tag_filter is not None:
            assert isinstance(tag_filter, type(self.tag_filter))
            self.tag_filter = tag_filter

        if text_adapter is not None:
            assert isinstance(text_adapter, type(self.text_adapter))
            self.text_adapter = text_adapter

        if text_filter is not None:
            assert isinstance(text_filter, type(self.text_filter))
            self.text_filter = text_filter

    def adapt_text(self, text):

        new_text = self.text_adapter(text)

        return new_text

    def adapt_tags(self, tags):

        new_tags = list(self.tag_adapter(t)
                        for t in tags
                        if self.tag_filter(t))

        return new_tags

    def adapt_documents(self, documents):

        new_pairs = list(self.adapt_document(p)
                         for p in documents
                         if self.text_filter(p.text))

        return new_pairs