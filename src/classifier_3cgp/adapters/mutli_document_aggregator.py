# coding=utf-8
from src.classifier_3cgp.models.corpus import Corpus

__author__ = 'Sébastien Rigaux'


class MultiCorpusAggregator:
    """
    A corpus aggregator
    """

    def __init__(self):
        pass

    @staticmethod
    def aggregate_documents(corpora):
        """
        Aggregates several corpora

        :param corpora: The corpora to aggregate
        :type corpora: list[Corpus]

        :return: The aggregate corpus
        :rtype: Corpus
        """
        assert isinstance(corpora, list)

        new_documents = []

        for corpus in corpora:
            new_documents.extend(corpus.documents)

        new_corpus = Corpus(new_documents)

        return new_corpus