# coding=utf-8
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'


class PipelineCorpusAdapter(CorpusAdapter):
    """
    Aggregates several ``CorpusAdapter``
    """

    def __init__(self, adapters=None):
        """

        :param adapters: A list of adapters
        :type adapters: list[CorpusAdapter]
        """

        if not adapters:
            adapters = []

        super(PipelineCorpusAdapter, self).__init__()

        for a in adapters:
            assert isinstance(a, CorpusAdapter)

        self._adapters = adapters

    @property
    def adapters(self):
        """
        :return: The adapters
        :rtype: list[CorpusAdapter]
        """
        return self._adapters

    @adapters.setter
    def adapters(self, adapters):
        """
        :param adapters: The adapters
        :type adapters: list[CorpusAdapter]
        """
        self._adapters = adapters

    def adapt_corpus(self, document):
        return reduce(lambda d, a: a.adapt_corpus(d), self.adapters, document)