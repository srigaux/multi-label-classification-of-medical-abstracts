# coding=utf-8
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'


class TagsFilterCorpusAdapter(CorpusAdapter):
    """
    A corpus adapter which allow to filter the tags of a corpus
    """

    def __init__(self, predicate):
        """

        :param predicate: the tag filter predicate
        :type predicate: (str) -> bool
        """
        CorpusAdapter.__init__(self)

        self.filter = lambda s: True

        if predicate is not None:
            assert isinstance(predicate, type(self.filter))
            self.tag_filter = predicate

    def adapt_tags(self, tags):
        new_tags = list(t
                        for t in tags
                        if self.filter(t))

        return new_tags
