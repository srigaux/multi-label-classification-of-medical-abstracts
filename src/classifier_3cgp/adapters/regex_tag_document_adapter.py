# coding=utf-8
import re
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter

__author__ = 'Sébastien Rigaux'


class RegexTagCorpusAdapter(CorpusAdapter):
    """
    A regex tag adapter
    (returns the first group found in the regex, else ``None``)
    """

    def __init__(self, pattern, flags=None):
        """
        :param pattern: The regex pattern
        :type pattern: str
        :param flags: The regex flags
        :type flags: int
        """
        super(RegexTagCorpusAdapter, self).__init__()

        flags = flags or re.IGNORECASE
        self._regex = re.compile(pattern, flags)

    def adapt_tag(self, tag):

        found = self._regex.findall(tag)

        return found[0] if found else None