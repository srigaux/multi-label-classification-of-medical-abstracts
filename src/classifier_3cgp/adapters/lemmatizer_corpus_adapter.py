# coding=utf-8
import os
from nltk.stem.wordnet import WordNetLemmatizer as NltkWordNetLemmatizer
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize

from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter
from src.treetaggerwrapper.treetaggerwrapper import TreeTagger

__author__ = 'Sébastien Rigaux'


class TreeTaggerWord:
    """
    Represents a TreeTagger word result
    http://www.fabienpoulard.info/post/2011/01/09/Python-et-Tree-Tagger
    """

    def __init__(self, triplet):
        """
        Init a TreeTaggerWord

        :param triplet: A triplet of string (word, postag, lemma)
        :type triplet: (str, str, str)
        """
        self.word, self.postag, self.lemma = triplet
        self.lemma = self.lemma.split('|')[0]

    @staticmethod
    def parse(line):
        """
        Parse a results line into a ``TreeTaggerWord``

        :param line: The line to parse
        :type line: str

        :return: The ``TreeTaggerWord``
        :rtype: TreeTaggerWord
        """
        triplet = line.split('\t')
        if not isinstance(triplet, (list, tuple)) or not len(triplet) == 3:
            return None

        return TreeTaggerWord(triplet)

    @staticmethod
    def parse_all(output):
        """
        Parses the result of TreeTagger and returns the TreeTaggerWords

        :param output: The TreeTagger results
        :type output: str

        :return: The list of ``TreeTaggerWord``
        :rtype: list[TreeTaggerWord]
        """
        return list(TreeTaggerWord.parse(line) for line in output)


class TreeTaggerLemmatizer:
    """
    A TreeTagger lemmatizer
    """

    def __init__(self, word_filter=None, lang='fr'):
        """
        Init a TreeTaggerLemmatizer

        :param word_filter: A word filter predicate
        :type word_filter: (str) -> bool

        :param lang: The lemmatizer language
        :type lang: str
        """

        tag_dir = os.path.join(os.path.dirname(__file__), '../../../libs/tree-tagger-MacOSX-3.2-intel')

        self._tagger = TreeTagger(
            TAGLANG=lang,
            TAGDIR=tag_dir,
            TAGINENC='utf-8',
            TAGOUTENC='utf-8'
        )
        self.word_filter = word_filter or (lambda w: True)

    def lemmatize(self, text):
        """
        Lemmatize the string ``text``

        :param text: The string to lemmatize
        :type text: str

        :return: The lemmatized string
        :rtype: str
        """
        output = self._tagger.TagText(text)
        tree_tagger_words = TreeTaggerWord.parse_all(output)

        return " ".join(
            w.lemma
            if w.lemma != '<unknown>'
            # and w.postag != 'NUM'
            else w.word

            for w in tree_tagger_words
            if w is not None and self.word_filter(w))


class WordNetLemmatizer:
    """
    A WordNet lemmatizer
    """
    def __init__(self, word_filter=None, pos_tag=True):
        """
        Init a WordNet lemmatizer

        :param word_filter: A word filter predicate
        :type word_filter: (str) -> bool

        :param pos_tag: ``true`` is should use PosTagging (NltkWordNetLemmatizer), else ``false``
        :type pos_tag: bool
        """
        self.word_filter = word_filter or (lambda w: True)
        self._lemmatizer = NltkWordNetLemmatizer()
        self.pos_tag=pos_tag

    def lemmatize(self, str):
        """
        Lemmatize a string

        :param str: The string to lemmatize
        :type str: str

        :return: The lemmatized string
        :rtype: str
        """
        if self.pos_tag:
            return self._lemmatize_with_pos_tag(str)
        else:
            return self._lemmatize_without_pos_tag(str)

    def _lemmatize_without_pos_tag(self, text):
        tokens = word_tokenize(text)
        return " ".join(
            tgw.lemma
            for tgw in
            (
                TreeTaggerWord((w, 'N', self._lemmatizer.lemmatize(w)))
                for w in tokens
            )
            if tgw is not None and self.word_filter(tgw))

    def _lemmatize_with_pos_tag(self, text):
        tokens = word_tokenize(text)
        tagged = pos_tag(tokens)

        return " ".join(
            tgw.lemma
            for tgw in
            (
                TreeTaggerWord((w, pos, self._lemmatizer.lemmatize(w, self.__get_wordnet_pos(pos))))
                for w, pos in tagged
            )
            if tgw is not None and self.word_filter(tgw))

    @staticmethod
    def __get_wordnet_pos(treebank_tag):

        if treebank_tag.startswith('J'):
            return 'a'
        elif treebank_tag.startswith('V'):
            return 'v'
        elif treebank_tag.startswith('N'):
            return 'n'
        elif treebank_tag.startswith('R'):
            return 'r'
        else:
            return 'n'


class LemmatizerCorpusAdapter(CorpusAdapter):
    """
    A corpus adapter which lemmatize the texts
    """

    def __init__(self, lemmatizer=None):
        """
        Init a LemmatizerCorpusAdapter

        :param lemmatizer: The lemmatizer to use (or ``TreeTaggerLemmatizer``)
        :type lemmatizer: TreeTaggerLemmatizer | WordNetLemmatizer
        """
        CorpusAdapter.__init__(self)
        self.lemmatizer = lemmatizer or TreeTaggerLemmatizer()

    def adapt_text(self, text):
        new_text = self.lemmatizer.lemmatize(text)
        return new_text
