# coding=utf-8
from random import Random

from src.classifier_3cgp.adapters.mutli_document_aggregator import MultiCorpusAggregator
from src.classifier_3cgp.importers.atlas_ti.stored_hu_xml_importer import StoredHUXmlImporter
from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.tester import BaseTester

__author__ = 'Sébastien Rigaux'


class Tester(BaseTester):
    __clermont_parser = None
    __lille_parser = None

    def __init__(self, filename, description, stored_hu_document_adapter=None, document_adapters=None, pipeline=None,
                 test_percent=0.1, random=Random(0), force=False):
        super(Tester, self).__init__(filename, description, document_adapters=document_adapters, pipeline=pipeline,
                                     test_percent=test_percent, random=random, force=force)
        self.stored_hu_document_adapter = stored_hu_document_adapter

    def _parse(self):
        """
        :rtype : Corpus
        """

        adapter = self.stored_hu_document_adapter

        Tester.__clermont_parser = StoredHUXmlImporter('../data/atlas_ti/Clermont.xml', adapter)
        Tester.__lille_parser = StoredHUXmlImporter('../data/atlas_ti/Lille.xml', adapter)

        clermont_document = Tester.__clermont_parser.import_corpus()
        lille_document = Tester.__lille_parser.import_corpus()

        document = \
            MultiCorpusAggregator.aggregate_documents([clermont_document, lille_document])

        return document

    @staticmethod
    def stop_words():
        return [u'alors', u'au', u'aucuns', u'aussi', u'autre', u'avant', u'avec', u'avoir', u'bon', u'car', u'ce',
                u'cela', u'ces', u'ceux', u'chaque', u'ci', u'comme', u'comment', u'dans', u'des', u'du', u'dedans',
                u'dehors', u'depuis', u'devrait', u'doit', u'donc', u'dos', u'début', u'elle', u'elles', u'en',
                u'encore',
                u'essai', u'est', u'et', u'eu', u'fait', u'faites', u'fois', u'font', u'hors', u'ici', u'il', u'ils',
                u'je', u'juste', u'la', u'le', u'les', u'leur', u'là', u'ma', u'maintenant', u'mais', u'mes', u'mine',
                u'moins', u'mon', u'mot', u'même', u'ni', u'nommés', u'notre', u'nous', u'ou', u'où', u'par', u'parce',
                u'pas', u'peut', u'peu', u'plupart', u'pour', u'pourquoi', u'quand', u'que', u'quel', u'quelle',
                u'quelles', u'quels', u'qui', u'sa', u'sans', u'ses', u'seulement', u'si', u'sien', u'son', u'sont',
                u'sous', u'soyez', u'sujet', u'sur', u'ta', u'tandis', u'tellement', u'tels', u'tes', u'ton', u'tous',
                u'tout', u'trop', u'très', u'tu', u'voient', u'vont', u'votre', u'vous', u'vu', u'ça', u'étaient',
                u'état', u'étions', u'été', u'être']
