# coding=utf-8
from abc import abstractmethod
from json import dumps
import os
from random import Random

from scipy.stats.mstats_basic import sem
from sklearn import metrics
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.metrics import f1_score, accuracy_score, jaccard_similarity_score, precision_score, recall_score, \
    hamming_loss
from sklearn.multiclass import _ConstantPredictor
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np

from nltk.corpus import stopwords

from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.models.result import Result, ArticleTestResult, LabelArticleTestResult, TestResult, \
    StatsResult, \
    JsonEncoder, TrainResult, MostImportantFeaturesResult, MostImportantFeatureResult

__author__ = 'Sébastien Rigaux'


class BaseTester(object):
    def __init__(self, filename, description,
                 document_adapters=None,
                 pipeline=None,
                 test_percent=0.1,
                 random=Random(0),
                 force=False):

        self.filename = filename
        self.description = description
        self.document_adapters = document_adapters or []
        self.pipeline = pipeline
        self.test_percent = test_percent
        self.random = random
        self.force = force

    @abstractmethod
    def _parse(self):
        """
        :rtype : Corpus
        """
        pass

    def __adapt(self, document):
        """
        :rtype : Corpus
        :param document: Corpus
        :return: Corpus
        """

        adapted_document = document
        for adapter in self.document_adapters:
            adapted_document = adapter.adapt_corpus(adapted_document)

        return adapted_document

    @staticmethod
    def evaluate_cross_validation(clf, X, y, K):

        cv = KFold(len(y), K, shuffle=True, random_state=0)

        # by default the score used is the one returned by score method of the estimator (accuracy)
        scores = cross_val_score(clf, X, y, cv=cv, scoring='f1')
        print scores
        print "Mean score: {0:.3f} (+/-{1:.3f})".format(
            np.mean(scores), sem(scores))

    def test(self):

        if not self.force and os.path.isfile(self.filename + ".json"):
            print self.filename, "exists --> ignored"
            return
        else:
            print self.filename, "..."

        result = Result(self.filename, self.description)

        document = self._parse()
        adapted_document = self.__adapt(document)

        # Randomize

        # self.random.shuffle(adapted_document.documents)

        documents_texts = adapted_document.texts()
        documents_tags = adapted_document.tags()

        # Binarize

        mlb = MultiLabelBinarizer()
        documents_binary_labels = mlb.fit_transform(documents_tags)

        # Split

        size = int(len(documents_texts) * self.test_percent)

        train_texts = documents_texts[size:]
        train_binary_labels = documents_binary_labels[size:]

        test_texts = documents_texts[:size]
        test_binary_labels = documents_binary_labels[:size]

        # Classify

        classifier = self.pipeline
        clf = self.pipeline.named_steps['clf']

        print 'Trains classifier ...'

        classifier.fit(train_texts, train_binary_labels)

        print 'Tests classifier ...'

        predicted_binary_labels = classifier.predict(test_texts)

        try:
            predicted_labels_proba = classifier.predict_proba(test_texts)
        except AttributeError:
            predicted_labels_proba = predicted_binary_labels

        categories = mlb.classes_

        vectorizer = self.pipeline.named_steps['vectorizer']

        result.stats = StatsResult(
            # http://scikit-learn.org/stable/modules/classes.html#classification-metrics
            accuracy_score=accuracy_score(test_binary_labels, predicted_binary_labels),
            precision_score=precision_score(test_binary_labels, predicted_binary_labels, categories, average='samples'),
            recall_score=recall_score(test_binary_labels, predicted_binary_labels, categories, average='samples'),
            f1_score=f1_score(test_binary_labels, predicted_binary_labels, categories, average='samples'),
            jaccard_similarity_score=jaccard_similarity_score(test_binary_labels, predicted_binary_labels),
            hamming_loss=hamming_loss(test_binary_labels, predicted_binary_labels, categories)
        )

        result.test = TestResult([])
        for text, probas, rlabels in zip(test_texts, predicted_labels_proba, test_binary_labels):
            article = ArticleTestResult(text=text)

            article.labels = list(
                LabelArticleTestResult(
                    title=l,
                    proba=p,
                    selected=True if (r == 1) else False)
                for p, r, l in zip(probas, rlabels, mlb.classes_)
            )

            result.test.articles.append(article)

        result.test.classification_report = metrics.classification_report(
            test_binary_labels, predicted_binary_labels, target_names=categories)

        # tp is the number of true positives and fp the number of false positives.

        # The precision is the ratio tp / (tp + fp)
        # The precision is intuitively the ability of the classifier not to label as positive a sample that is negative.

        # The recall is the ratio tp / (tp + fn)
        # The recall is intuitively the ability of the classifier to find all the positive samples.

        # The F-beta score can be interpreted as a weighted harmonic mean of the precision and recall, [0.0-1.0]
        # The F-beta score weights recall more than precision by a factor of beta.
        # beta == 1.0 means recall and precision are equally important.

        # The support is the number of occurrences of each class in y_true.

        result.train = TrainResult(
            self.show_most_informative_features(mlb.classes_, vectorizer, clf, 20)
        )

        json = dumps(result, cls=JsonEncoder, indent=4)

        with open(self.filename + ".json", 'w') as f:
            f.write(json.encode('utf8'))

    @staticmethod
    def show_most_informative_features(labels, vectorizer, clf, n=20):

        results = []

        feature_names = vectorizer.get_feature_names()

        for i, l in enumerate(labels):

            result = MostImportantFeaturesResult(l, [], [])

            estimator = clf.estimators_[i]

            if hasattr(estimator, 'coef_'):
                coefs_with_fns = sorted(zip(estimator.coef_[0], feature_names))
                top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
                for (coef_1, fn_1), (coef_2, fn_2) in top:
                    result.against_features.append(MostImportantFeatureResult(fn_1, coef_1))
                    result.for_features.append(MostImportantFeatureResult(fn_2, coef_2))

            elif isinstance(estimator, _ConstantPredictor):
                if estimator.y_.shape[0] == 0:
                    result.for_features.append(MostImportantFeatureResult("Always true", 1.0))
                else:
                    result.against_features.append(MostImportantFeatureResult("Always false", -1.0))

            results.append(result)

        return results
