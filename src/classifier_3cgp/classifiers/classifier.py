# coding=utf-8
from abc import abstractmethod
import os
import re
from bs4 import BeautifulSoup
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MultiLabelBinarizer
from src.classifier_3cgp.adapters.corpus_adapter import CorpusAdapter
from src.classifier_3cgp.adapters.text_corpus_adapter import TextCorpusAdapter
from src.classifier_3cgp.importers.joblib_importer import JoblibImporter
from src.classifier_3cgp.models.corpus import Corpus

__author__ = 'Sébastien Rigaux'


class Classifier(object):
    """
    Classifier
    """

    def __init__(self, corpus_bin_path, clf_bin_path):
        """
        :param corpus_bin_path: The path of the cached corpus
        :param clf_bin_path:  The path of the cached classifier
        """

        super(Classifier, self).__init__()

        self.corpus_bin_path = corpus_bin_path
        self.clf_bin_path = clf_bin_path
        self.clf = None
        """:type: Pipeline"""

    def predict(self, text):
        """
        Predicts the categories

        :param text: The text to analyse
        :rtype text: str

        :return: The categories
        :rtype: list[str]
        """
        corpus = Corpus.from_text(text)

        # Removes excessive white spaces
        html_white_space_regex = re.compile(r"(\s+)", re.MULTILINE)
        
        # Strips html tags
        strip_html_text_adapter = TextCorpusAdapter(
            lambda s: re.sub(html_white_space_regex, " ", BeautifulSoup(s).get_text(strip=True)))

        corpus = strip_html_text_adapter.adapt_corpus(corpus)

        adapter = self._corpus_adapter()
        if adapter is not None:
            corpus = adapter.adapt_corpus(corpus)

        adapted_text = corpus.texts()[0]

        mlb, clf = self._load()

        categories_bin = clf.predict([adapted_text])

        categories = mlb.inverse_transform(categories_bin)

        return categories[0]

    def _create(self):
        """
        Create the classifier

        :return: the label binarizer and the classifier
        :rtype (MultiLabelBinarizer, Pipeline)
        """

        corpus = self._corpus()

        texts = corpus.texts()
        tags = corpus.tags()

        # Binarize the tags

        mlb = MultiLabelBinarizer()
        corpus_binary_labels = mlb.fit_transform(tags)

        steps = [
            ("vect", self._vectorizer()),
            ("clf", self._classifier())
        ]

        fi = self._fi()
        if fi is not None:
            steps.insert(1, ("fs", fi))

        clf = Pipeline(steps)

        clf.fit(texts, corpus_binary_labels)

        return mlb, clf

    def _save(self, mlb, clf):
        path = os.path.join(os.path.dirname(__file__), self.clf_bin_path)

        joblib.dump((mlb, clf), path)

    def _load(self):
        """
        :return: The label binarizer and the pipeline
        :rtype: (MultiLabelBinarizer, Pipeline)
        """

        if self.clf is None:

            try:
                path = os.path.join(os.path.dirname(__file__), self.clf_bin_path)
                (self.mlb, self.clf) = joblib.load(path)

            except:
                self.mlb, self.clf = self._create()
                self._save(self.mlb, self.clf)

        return self.mlb, self.clf

    def _corpus(self):
        """
        Get the corpus

        :return: The corpus
        :rtype: Corpus
        """
        path = os.path.join(os.path.dirname(__file__), self.corpus_bin_path)
        return JoblibImporter(path).import_corpus()

    @abstractmethod
    def _vectorizer(self):
        """
        Get the vectorizer

        :return: The vectorizer
        """
        pass

    @abstractmethod
    def _classifier(self):
        """
        Get the classifier implementation

        :return: The classifier
        """
        pass

    @abstractmethod
    def _fi(self):
        """
        Get the feature identification method

        :return: The fi method
        """
        pass

    @abstractmethod
    def _corpus_adapter(self):
        """
        Get the corpus adapter

        :return: A corpus adapter
        :rtype: CorpusAdapter
        """
        pass