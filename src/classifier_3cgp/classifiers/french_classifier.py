# coding=utf-8
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.multiclass import OneVsRestClassifier
from src.classifier_3cgp.adapters.lemmatizer_corpus_adapter import LemmatizerCorpusAdapter, TreeTaggerLemmatizer
from src.classifier_3cgp.classifiers.classifier import Classifier
from src.classifier_3cgp.tester_french import Tester

__author__ = 'Sébastien Rigaux'


class FrenchClassifier(Classifier):
    def __init__(self):
        super(FrenchClassifier, self).__init__(
            corpus_bin_path="../../../data/documents/french_filtered_lemmatized.bin",
            clf_bin_path="../../../data/classifiers/french_clf.bin")

    def _classifier(self):
        return OneVsRestClassifier(SGDClassifier(
            loss='log',
            penalty='l2',
            alpha=0.1,
            class_weight='auto'
        ), n_jobs=-1)

    def _fi(self):
        return None

    def _vectorizer(self):
        return CountVectorizer(
            max_df=1.0,
            min_df=1,
            max_features=None,
            ngram_range=(1, 1),
            stop_words=Tester.stop_words()
        )

    def _corpus_adapter(self):
        return LemmatizerCorpusAdapter(
            TreeTaggerLemmatizer(word_filter=lambda w: w.postag.startswith(('A', 'NAM', 'NOM', 'VER')))
        )
