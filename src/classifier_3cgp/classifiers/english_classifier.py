# coding=utf-8
import re

from sklearn.decomposition.truncated_svd import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm.classes import LinearSVC

from src.classifier_3cgp.adapters.lemmatizer_corpus_adapter import LemmatizerCorpusAdapter, WordNetLemmatizer
from src.classifier_3cgp.classifiers.classifier import Classifier

__author__ = 'Sébastien Rigaux'


class EnglishClassifier(Classifier):
    def __init__(self):
        super(EnglishClassifier, self).__init__(
            corpus_bin_path="../../../data/documents/english_filtered_lemmatized.bin",
            clf_bin_path="../../../data/classifiers/english_clf.bin")

    def _classifier(self):
        return OneVsRestClassifier(LinearSVC(
            C=0.01,
            penalty='l2',
            loss='l2',
            class_weight='auto',
            dual=True
        ), n_jobs=-1)

    def _fi(self):
        return TruncatedSVD(n_components=1000)

    def _vectorizer(self):
        return TfidfVectorizer(
            max_df=1.0,
            min_df=1,
            max_features=None,
            ngram_range=(1, 1),
            sublinear_tf=True,
            norm='l2',
            stop_words='english'
        )

    def _corpus_adapter(self):
        only_char_regex = re.compile("^[a-zA-ZÀ-ÿ]*$")
        return LemmatizerCorpusAdapter(
            WordNetLemmatizer(
                word_filter=lambda w: (only_char_regex.match(w.word)
                                       and
                                       w.postag.startswith(('FW', 'J', 'MD', 'N', 'R', 'V', 'W')))
            )
        )
