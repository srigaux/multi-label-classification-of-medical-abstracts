# coding=utf-8
from time import time
import warnings
from sklearn.base import ClassifierMixin

from sklearn.cross_validation import KFold, cross_val_score
from sklearn.decomposition.truncated_svd import TruncatedSVD
from sklearn.ensemble.weight_boosting import AdaBoostClassifier
from sklearn.feature_extraction.text import VectorizerMixin
from sklearn.feature_selection.univariate_selection import SelectKBest
from sklearn.grid_search import ParameterGrid
from sklearn.metrics.metrics import UndefinedMetricWarning
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np

from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.utils.dense_transformer import DenseTransformer

__author__ = 'Sébastien Rigaux'


class TestGenerator(object):
    """
    Generate a list of configured Tests according
    to the corpus, the vectorizer, the feature_selection method
    and the ML methods, and all their parameters given
    """

    def __init__(self,
                 language,
                 corpus,
                 vectorizer,
                 feature_selection,
                 classifier):
        """
        :type self: TestGenerator

        :param language: The language of the corpus (``fr`` | ``en``)
        :type language: str

        :param corpus: The corpus used for the test (and its name)
        :type corpus: (str, Corpus)

        :param vectorizer: The vectorizer used for the test (and its name and parameters)
        :type vectorizer: (str, (VectorizerMixin, dict))

        :param feature_selection: The FS algorithm to use (and its name and parameters)
        :type feature_selection: (str, (SelectKBest | TruncatedSVD, dict))

        :param classifier: The ML method to use (and its name and parameters)
        :type classifier: (str, (OneVsRestClassifier, dict))

        """
        super(TestGenerator, self).__init__()

        self.language = language
        self.corpus = corpus
        self.vectorizer = vectorizer
        self.feature_selection = feature_selection
        self.classifier = classifier

    def generate(self, skip_delegate=lambda h: True):
        """
        Returns a generator which generates a result dictionary and the hash
        of each test not skipped (one for each parameters combination)


        :param skip_delegate: The delegate which decides to skip or perform the test according to its hash
        :type skip_delegate: (int) -> bool

        :return: A generator which returns a result dictionary and the hash of each test not skipped
        :rtype:  __generator[dict[str, str], int]
        """
        corpus = self.corpus[1]

        texts = corpus.texts()
        tags = corpus.tags()

        # Binarize the tags

        mlb = MultiLabelBinarizer()
        corpuss_binary_labels = mlb.fit_transform(tags)

        # Retains the pipeline steps
        steps = [
            ("vect", self.vectorizer[1][0]),
            ("clf", self.classifier[1][0])
        ]

        # Updates the parameters used to identify the test
        params = {}
        params.update(self.vectorizer[1][1])
        params.update(self.classifier[1][1])

        # if adaBoost --> transforms the sparse matrix to dense matrix
        if isinstance(self.classifier[1][0].estimator, AdaBoostClassifier):
            steps.insert(1, ("to_dense", DenseTransformer()))

        # if the test has a feature selection method --> adds it to the pipeline's steps
        if self.feature_selection[1] is not None:
            steps.insert(1, ("fs", self.feature_selection[1][0]))
            params.update(self.feature_selection[1][1])

        # Creates the pipeline
        pipeline = Pipeline(steps)

        # Uses a 5-fold cross-validation generator
        cv = KFold(len(corpuss_binary_labels), n_folds=5, shuffle=True, random_state=0)

        warnings.filterwarnings("ignore", ".*is present in all training examples.*")
        warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

        # Iterates all parameters combination
        for parameters in list(ParameterGrid(params)):

            # Calculates the hash
            h = self.hash(parameters)

            # Ignores the skipped tests
            if skip_delegate(h):
                continue

            # Generates a result dictionary
            result = self.dict_repr(parameters)

            # Changes the stop words if french corpus
            if self.language == "fr":
                parameters["vect__stop_words"] = self.__french_stop_words()
                result["vect__stop_words"] = "fr"

            # Prints the current test parameters
            print "=" * 40, "\n\t" + "\n\t".join(["%-25s %s" % (k, v) for k, v in sorted(result.items())])

            # Configures the pipeline to use these parameters
            pipeline.set_params(**parameters)

            t0 = time()

            # Actually performs the current test with 5-fold cross-validation
            f1_scores = cross_val_score(pipeline,
                                        texts,
                                        corpuss_binary_labels,
                                        scoring="f1",
                                        cv=cv)

            cross_val_time = time() - t0

            # Update the result dict with the result statistics
            result.update({
                'f1 mean': np.mean(f1_scores),
                'f1 std': np.std(f1_scores),
                'cross_val_time': cross_val_time,
                'n_samples': len(texts),
                'hash': h
            })

            print "\n\t --> \t", "f1:", np.mean(f1_scores), "\ttime:", cross_val_time, "\n\n"

            # Yields the result dictionary and its hash
            yield result, h

    def dict_repr(self, params=None):
        """
        Returns the dictionary representation of the test

        :Details: Used to generate the hash of the test

        :param params: Extra parameters used to identify a test
        :type params: dict[str, str]

        :return: The dictionary which contains all the keys/values used to identify a test
        :rtype: dict[str, str]
        """

        dictionnary = {
            'lang': self.language,
            'doc': self.corpus[0],
            'vect': self.vectorizer[0],
            'fs': self.feature_selection[0],
            'clf': self.classifier[0]
        }

        # Add extra params
        if params is not None:
            dictionnary.update(params)

        return dictionnary

    def hash(self, params=None):
        """
        Generate a hash for the test allowing to identify it

        :param params: The extra params used in the hash
        :type params: dict[str, str]

        :return: The hash
        :rtype: int
        """

        dict_repr = self.dict_repr(params)
        str_key = "#".join(["%s=%s" % (k, v) for k, v in sorted(dict_repr.items())])

        h = hash(str_key)

        return h

    @staticmethod
    def __french_stop_words():
        """
        The french stop words

        :return: The french stop words
        :rtype: list[unicode]
        """
        return [u'alors', u'au', u'aucuns', u'aussi', u'autre', u'avant', u'avec', u'avoir', u'bon', u'car', u'ce',
                u'cela', u'ces', u'ceux', u'chaque', u'ci', u'comme', u'comment', u'dans', u'des', u'du', u'dedans',
                u'dehors', u'depuis', u'devrait', u'doit', u'donc', u'dos', u'début', u'elle', u'elles', u'en',
                u'encore',
                u'essai', u'est', u'et', u'eu', u'fait', u'faites', u'fois', u'font', u'hors', u'ici', u'il', u'ils',
                u'je', u'juste', u'la', u'le', u'les', u'leur', u'là', u'ma', u'maintenant', u'mais', u'mes', u'mine',
                u'moins', u'mon', u'mot', u'même', u'ni', u'nommés', u'notre', u'nous', u'ou', u'où', u'par', u'parce',
                u'pas', u'peut', u'peu', u'plupart', u'pour', u'pourquoi', u'quand', u'que', u'quel', u'quelle',
                u'quelles', u'quels', u'qui', u'sa', u'sans', u'ses', u'seulement', u'si', u'sien', u'son', u'sont',
                u'sous', u'soyez', u'sujet', u'sur', u'ta', u'tandis', u'tellement', u'tels', u'tes', u'ton', u'tous',
                u'tout', u'trop', u'très', u'tu', u'voient', u'vont', u'votre', u'vous', u'vu', u'ça', u'étaient',
                u'état', u'étions', u'été', u'être']