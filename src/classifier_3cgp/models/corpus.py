# coding=utf-8
from src.classifier_3cgp.models.document import Document

__author__ = 'Sébastien Rigaux'


class Corpus:
    """
    A corpus (Annotated set of docuemnts)
    """

    def __init__(self, documents=None):
        """
        :param documents: The corpus's documents
        :type documents: list[Document]
        """

        if not documents:
            documents = []

        self.documents = documents

    def add_document(self, document):
        """
        Add a document to the corpus

        :param document: The document to add
        :type document: Document
        """
        self.documents.append(document)

    def texts(self):
        """
        :return: The texts of the corpus
        :rtype: list[str]
        """
        return [t.text for t in self.documents]

    def tags(self):
        """
        :return: The tags of the corpus
        :rtype: list[list[str]]
        """
        return [t.tags for t in self.documents]

    def unique_tags(self):
        """
        :return: The set of tags of the corpus
        :rtype: list[str]
        """
        return sorted(set().union(self.tags()))

    @staticmethod
    def from_text(text):
        """
        Creates a corpus with a unique not annotated abstract

        :param text: The abstract
        :type text: str

        :return: The corpus
        :rtype: Corpus
        """

        document = Document(text)
        corpus = Corpus([document])

        return corpus