# coding=utf-8
__author__ = 'Sébastien Rigaux'

from json import JSONEncoder


class JsonEncoder(JSONEncoder):
    """
    Default JSON Encoder (Encode the object's field ``__dict__``)
    """
    def default(self, o):
        if hasattr(o, "__dict__"):
            return o.__dict__
        return o


class Result(object):
    """
    A Machine test result
    """

    def __init__(self, name, description, stats=None, train=None, test=None):
        """
        :param name: The machine's name
        :type name: str

        :param description: The machine's description
        :type description: str

        :param stats: The machine's stats results
        :type stats: StatsResult

        :param train: The machine's train results
        :type train: TrainResult

        :param test: The machine's test results
        :type test: TestResult
        """

        super(Result, self).__init__()

        self.name = name
        self.description = description
        self.stats = stats
        self.train = train
        self.test = test


class StatsResult(object):
    """
    A machine statistics result
    """

    def __init__(self, f1_score, accuracy_score=None, precision_score=None, recall_score=None,
                 jaccard_similarity_score=None, hamming_loss=None):
        """
        :param f1_score: The machine's F1 score
        :type f1_score: float

        :param accuracy_score: The machine's accuracy score
        :type : float

        :param precision_score: The machine's precision score
        :type accuracy_score: float

        :param recall_score: The machine's recall score
        :type recall_score: float

        :param jaccard_similarity_score: The machine's Jaccard similarity score
        :type jaccard_similarity_score: float

        :param hamming_loss: The machine's Hamming loss
        :type hamming_loss: float
        """
        super(StatsResult, self).__init__()

        self.f1_score = f1_score
        self.accuracy_score = accuracy_score
        self.precision_score = precision_score
        self.recall_score = recall_score
        self.jaccard_similarity_score = jaccard_similarity_score
        self.hamming_loss = hamming_loss


class TrainResult(object):

    def __init__(self, most_important_features=None):
        """
        :param most_important_features: The most important features
        :type most_important_features: list[MostImportantFeaturesResult]
        """
        super(TrainResult, self).__init__()

        self.most_important_features = most_important_features or []


class MostImportantFeaturesResult(object):
    """
    Retains the best discriminant features for a category
    """

    def __init__(self, label, for_features=None, against_features=None):
        """
        :param label: The category's label
        :type label: str

        :param for_features: The best discriminant features to classify in the category
        :type for_features: list[MostImportantFeatureResult]

        :param against_features: The best discriminant features to not classify in the category
        :type against_features: list[MostImportantFeatureResult]
        """
        super(MostImportantFeaturesResult, self).__init__()

        self.label = label
        self.for_features = for_features or []
        self.against_features = against_features or []


class MostImportantFeatureResult(object):
    """
    Retains a feature and its coefficient
    """

    def __init__(self, name, coef):
        """
        :param name: The feature's name
        :type name: str

        :param coef: The feature's coefficient
        :type coef: float
        """
        super(MostImportantFeatureResult, self).__init__()

        self.name = name
        self.coef = coef


class TestResult(object):
    """
    Show the probabilities of each label for all tested abstracts
    that the abstract has to be classify in this label
    """
    def __init__(self, articles=None, classification_report=None):
        """
        :param articles: The tested abstracts
        :type articles: list[ArticleTestResult]

        :param: Classification report (Classify report for each labels)
        :type classification_report: str
        """
        super(TestResult, self).__init__()

        self.articles = articles or []
        self.classification_report = classification_report


class ArticleTestResult(object):
    """
    Show the probabilities of each label that the abstract
    has to be classify in it
    """
    def __init__(self, text, labels=None):
        """
        :param text: The abstract's content
        :type text: str

        :param labels: The label's probabilities
        :type labels: list[LabelArticleTestResult]
        """
        super(ArticleTestResult, self).__init__()

        self.text = text
        self.labels = labels or []


class LabelArticleTestResult(object):
    """
    Label and probabilities that it was selected
    """

    def __init__(self, title, proba, selected):
        """
        :param title: The label's title
        :type title: str

        :param proba: The label's probabilities to be selected
        :type proba: float

        :param selected: ``True`` if the label is selected, ``False`` otherwise
        :type selected: bool
        """
        super(LabelArticleTestResult, self).__init__()

        self.title = title
        self.proba = proba
        self.selected = selected

