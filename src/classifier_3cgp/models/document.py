# coding=utf-8
__author__ = 'Sébastien Rigaux'


class Document:

    def __init__(self, text, tags=None):
        """
        :param text: The document's text
        :type text:str

        :param tags: The document's tags (default: ``[]``)
        :type tags:list[str]
        """

        if not tags:
            tags = []

        if not isinstance(text, (unicode, str)):
            print type(text)

        assert isinstance(text, unicode) or isinstance(text, str)
        assert text is not None

        self.tags = tags
        """:type: list[str]"""

        self.text = text
        """:type: str"""

    def add_tag(self, tag):
        """
        Add a tag to the document

        :param tag: The tag to add
        :type tag: str
        """
        self.tags.append(tag)