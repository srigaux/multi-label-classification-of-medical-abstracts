# coding=utf-8
from random import Random

from nltk.corpus import stopwords

from src.classifier_3cgp.importers.wonca.wonca_importer import WoncaImporter
from src.classifier_3cgp.models.corpus import Corpus
from src.classifier_3cgp.tester import BaseTester

__author__ = 'Sébastien Rigaux'


class Tester(BaseTester):
    __wonca_importer = None
    ''':type : WoncaImporter'''

    def __init__(self, filename, description, document_adapters=None, pipeline=None, test_percent=0.1, random=Random(0),
                 force=False):
        super(Tester, self).__init__(filename, description, document_adapters=document_adapters, pipeline=pipeline,
                                     test_percent=test_percent, random=random, force=force)

    def _parse(self):
        """
        :rtype : Corpus
        """

        if Tester.__wonca_importer is None:
            Tester.__wonca_importer = \
                WoncaImporter(annotations_csv_filename='../data/wonca/paris_2007/annotations.csv',
                              articles_xml_directory_path='../data/wonca/paris_2007/xml_abstract')

        corpus = Tester.__wonca_importer.import_corpus()

        return corpus

    @staticmethod
    def stop_words():
        return stopwords.words("english")
