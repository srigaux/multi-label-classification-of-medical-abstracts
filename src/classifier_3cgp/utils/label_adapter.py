# coding=utf-8
from _warnings import warn

from src.classifier_3cgp.result import Result as R

__author__ = 'Sébastien Rigaux'


class LabelAdapter:
    """
    Helper class to bind method's, parameter's, ... names to a human readable string
    """

    parameters_bindings = {
        R.clf_c: u"Paramètre C",
        R.clf_alpha: u"Paramètre $\\alpha$",
        R.fs_n_components: u"Caractéristiques (N)"
    }

    lang_bindings = {
        R.lang_fr: u"FR",
        R.lang_en: u"EN"
    }

    doc_bindings = {
        R.doc_lemmatized: u"Lemmatisation",
        R.doc_lemmatized_filtered: u"Lemmatisation Filtrée",
        R.doc_stemmed: u"Racinisation.",
        R.doc_original: u"Aucune",
    }

    fs_bindings = {
        R.fs_bns: u"BNS",
        R.fs_chi2: u"Chi2", #u"$\\chi^2$",
        R.fs_mi: u"MI",
        R.fs_mi3: u"MI3", #u"MI$\\^3",
        R.fs_none: u"Aucune",
        R.fs_truncatedSVD: u"tSVD",
    }

    vect_bindings = {
        R.vect_presence: u"Présence",
        R.vect_occurrence: u"Occurence",
        R.vect_tf: u"TF",
        R.vect_tfidf: u"TF-IDF",
    }

    @staticmethod
    def label_for_parameter(param_name):

        if param_name in LabelAdapter.parameters_bindings:
            return LabelAdapter.parameters_bindings[param_name]

        warn("Undefined parameter label for '%s" % param_name)

        return param_name

    @staticmethod
    def label_for_lang(lang):

        if lang in LabelAdapter.lang_bindings:
            return LabelAdapter.lang_bindings[lang]

        warn("Undefined lang label for '%s" % lang)

        return lang

    @staticmethod
    def label_for_doc(doc):

        if doc in LabelAdapter.doc_bindings:
            return LabelAdapter.doc_bindings[doc]

        warn("Undefined doc label for '%s" % doc)

        return doc

    @staticmethod
    def label_for_fs(fs):

        if fs in LabelAdapter.fs_bindings:
            return LabelAdapter.fs_bindings[fs]

        warn("Undefined fs label for '%s" % fs)

        return fs

    @staticmethod
    def label_for_vect(vect):

        if vect in LabelAdapter.vect_bindings:
            return LabelAdapter.vect_bindings[vect]

        warn("Undefined vect label for '%s'" % vect)

        return vect