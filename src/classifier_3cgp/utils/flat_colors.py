# coding=utf-8
__author__ = 'Sébastien Rigaux'


class FlatColors:
    """
    Helper class which retains FlatUI colors

    :see: http://flatuicolors.com/

    """

    def __init__(self):
        pass

    turquoise = (26, 188, 156)
    emerald = (46, 204, 113)
    peter_river = (52, 152, 219)
    amethyst = (155, 89, 182)
    wet_asphalt = (52, 73, 94)
    green_sea = (22, 160, 133)
    nephritis = (39, 174, 96)
    belize_hole = (41, 128, 185)
    wisteria = (142, 68, 173)
    midnight_blue = (44, 62, 80)
    sun_flower = (241, 196, 15)
    carrot = (230, 126, 34)
    alizarin = (231, 76, 60)
    clouds = (236, 240, 241)
    concrete = (149, 165, 166)
    orange = (243, 156, 18)
    pumpkin = (211, 84, 0)
    pomegranate = (192, 57, 43)
    silver = (189, 195, 199)
    asbestos = (127, 140, 141)

    named_colors = dict(
        turquoise = turquoise,
        emerald = emerald,
        peter_river = peter_river,
        amethyst = amethyst,
        wet_asphalt = wet_asphalt,
        green_sea = green_sea,
        nephritis = nephritis,
        belize_hole = belize_hole,
        wisteria = wisteria,
        midnight_blue = midnight_blue,
        sun_flower = sun_flower,
        carrot = carrot,
        alizarin = alizarin,
        clouds = clouds,
        concrete = concrete,
        orange = orange,
        pumpkin = pumpkin,
        pomegranate = pomegranate,
        silver = silver,
        asbestos = asbestos,
    )

    colors = [
        turquoise,
        emerald,
        peter_river,
        amethyst,
        wet_asphalt,
        green_sea,
        nephritis,
        belize_hole,
        wisteria,
        midnight_blue,
        sun_flower,
        carrot,
        alizarin,
        clouds,
        concrete,
        orange,
        pumpkin,
        pomegranate,
        silver,
        asbestos,
    ]

    @staticmethod
    def to_hex(c, alpha=1.0):
        """
        Convert a color to Hex representation

        :param c: The color to convert
        :type c: (int,int,int)

        :param alpha: The alpha value of the generated color (default: ``1.0``)
        :type alpha: float

        :return: The Hex string
        :rtype: str
        """
        return "#%02x%02x%02x" % c + "%02x" % (alpha * 255.)

    @staticmethod
    def to_dec(c, alpha=1.0):
        """
        Convert a color to decimal representation

        :param c: The color to convert
        :type c: (int,int,int)

        :param alpha: The alpha value of the generated color (default: ``1.0``)
        :type alpha: float

        :return: The decimal tuple
        :rtype: (float,float,float,float)
        """
        r, g, b = c
        return (r/256., g/256., b/256., alpha)