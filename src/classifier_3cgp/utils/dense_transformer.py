# coding=utf-8
from sklearn.base import TransformerMixin, BaseEstimator

__author__ = 'Sébastien Rigaux'


class DenseTransformer(TransformerMixin, BaseEstimator):
    """
    SciKit Pipeline step which transform a SparseMatrix to a DenseMatrix

    :see: http://zacstewart.com/2014/08/05/pipelines-of-featureunions-of-pipelines.html
    """
    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self