# coding=utf-8
from warnings import warn
from src.classifier_3cgp.result import Result as R
from src.classifier_3cgp.utils.flat_colors import FlatColors as C

__author__ = 'Sébastien Rigaux'

class FlatColorsAdapter:
    """
    Helper class to bind method's names to a FlatUI color
    """

    def __init__(self):
        pass

    bindings = {

        u"Naïf": C.concrete,

        R.lang_fr: C.alizarin,
        R.lang_en: C.peter_river,

        R.doc_lemmatized: C.carrot,
        R.doc_lemmatized_filtered: C.peter_river,
        R.doc_stemmed: C.emerald,
        R.doc_original: C.wet_asphalt,

        R.vect_presence: C.carrot,
        R.vect_occurrence: C.wet_asphalt,
        R.vect_tf: C.turquoise,
        R.vect_tfidf: C.peter_river,

        R.fs_bns: C.sun_flower,
        R.fs_chi2: C.peter_river,
        R.fs_mi: C.orange,
        R.fs_mi3: C.pumpkin,
        R.fs_none: C.wet_asphalt,
        R.fs_truncatedSVD: C.turquoise,
    }

    @staticmethod
    def from_string(s, alpha=1.0):

        if s in FlatColorsAdapter.bindings:
            return C.to_dec(FlatColorsAdapter.bindings[s], alpha)

        warn("Undefined color for '%s" % s)

        return C.to_dec(C.colors[(hash(s) % len(C.colors))], alpha)
