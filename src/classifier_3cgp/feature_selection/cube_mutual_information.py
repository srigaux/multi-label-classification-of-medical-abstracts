# coding=utf-8
__author__ = 'Sébastien Rigaux'

from scipy.sparse import issparse
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import atleast2d_or_csr, array2d
from sklearn.utils.extmath import safe_sparse_dot

import numpy as np

def mi3(X, y):
    """Mutual information
    """

    X = atleast2d_or_csr(X)
    if np.any((X.data if issparse(X) else X) < 0):
        raise ValueError("Input X must be non-negative.")

    Y = LabelBinarizer().fit_transform(y)
    if Y.shape[1] == 1:
        Y = np.append(1 - Y, Y, axis=1)

    observed = safe_sparse_dot(Y.T, X)  # n_classes * n_features
    observed = np.asarray(observed, dtype=np.float64)

    class_count = array2d(Y.sum(axis=0), dtype=np.float64)
    terms_prob = array2d(X.mean(axis=0), dtype=np.float64)

    prob_t_given_c = observed
    prob_t_given_c /= class_count.T

    log_terms_prob = np.ma.log10(terms_prob)

    mutual_information = (np.ma.log10(prob_t_given_c) * 3)
    mutual_information -= log_terms_prob

    mutual_information = mutual_information.sum(axis=0)

    return mutual_information, mutual_information