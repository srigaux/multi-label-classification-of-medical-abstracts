# coding=utf-8
__author__ = 'Sébastien Rigaux'

from scipy.sparse import issparse
from scipy.stats import norm
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import atleast2d_or_csr, array2d
from sklearn.utils.extmath import safe_sparse_dot

import numpy as np

def bns(X, y):
    """Bi-Normal Separation
    """

    X = atleast2d_or_csr(X)
    if np.any((X.data if issparse(X) else X) < 0):
        raise ValueError("Input X must be non-negative.")

    Y = LabelBinarizer().fit_transform(y)
    if Y.shape[1] == 1:
        Y = np.append(1 - Y, Y, axis=1)

    observed = safe_sparse_dot(Y.T, X)  # n_classes * n_features
    observed = np.asarray(observed, dtype=np.float64)

    not_Y = np.mod(Y + 1,2)

    not_observed = safe_sparse_dot(not_Y.T, X)
    not_observed = np.asarray(not_observed, dtype=np.float64)

    class_count = array2d(Y.sum(axis=0), dtype=np.float64)

    class_tot = len(Y[0])

    not_class_count = class_tot - class_count

    prob_t_given_c = observed
    prob_t_given_not_c = not_observed

    with np.errstate(divide='ignore'):
        prob_t_given_c /= class_count.T
        prob_t_given_c = np.nan_to_num(prob_t_given_c)

        prob_t_given_not_c /= not_class_count.T
        prob_t_given_not_c[np.isnan(prob_t_given_not_c)] = 0

    bi_normal_separation = np.abs(norm.ppf(prob_t_given_c) - norm.ppf(prob_t_given_not_c))

    bi_normal_separation = bi_normal_separation.sum(axis=0)

    return bi_normal_separation, bi_normal_separation