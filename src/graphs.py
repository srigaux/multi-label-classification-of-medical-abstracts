# coding=utf-8
from itertools import groupby
import os
import numpy as np
from src.classifier_3cgp.result import Result as R
from src.classifier_3cgp.utils.flat_colors import FlatColors
import matplotlib.pyplot as plt
import inspect
from src.classifier_3cgp.utils.flat_colors_adapter import FlatColorsAdapter
from src.classifier_3cgp.utils.label_adapter import LabelAdapter

__author__ = 'Sébastien Rigaux'

import csv


def load_csv(filename='../results/results.csv'):
    """
    Load the results from an CSV file

    :param filename: The path to the file to load
    :type filename: str

    :return: The results
    :rtype: list[R]
    """
    if os.path.isfile(filename):
        with open(filename, 'r') as csvfile:
            return [R(**row) for row in csv.DictReader(csvfile)]


# Load the results
results = load_csv()

# region --- filter -----------------------------------------------------------

def _and(predicates):
    return lambda row: reduce(lambda b, l: b and l(row), predicates, True)


def _or(predicates):
    return lambda row: reduce(lambda b, l: b or l(row), predicates, False)


def _eq(column_name, value):
    return lambda row: getattr(row, column_name) == value

def _neq(column_name, value):
    return lambda row: getattr(row, column_name) != value

def _filter(rows, lang=None, doc=None, vect=None, fs=None, clf=None, other=None):
    predicates = []

    if other is not None:
        predicates = [other]

    if lang is not None: predicates.append(_eq(R.lang, lang))
    if doc is not None: predicates.append(_eq(R.doc, doc))
    if fs is not None: predicates.append(_eq(R.fs, fs))
    if vect is not None: predicates.append(_eq(R.vect, vect))
    if clf is not None: predicates.append(_eq(R.clf, clf))

    predicate = _and(predicates)

    return filter(predicate, rows)


# endregion

class Radar(object):
    """
    Helper class which plot a Radar graph
    """

    def __init__(self, fig, titles, labels, rect=None, start_degrees=90, total_degrees=360):
        if rect is None:
            rect = [0.05, 0.05, 0.95, 0.95]

        self.n = len(titles)
        self.angles = np.arange(start_degrees, start_degrees + total_degrees, float(total_degrees) / self.n)
        self.axes = [fig.add_axes(rect, projection="polar", label="axes%d" % i)
                     for i in range(self.n)]

        self.ax = self.axes[0]
        self.ax.set_thetagrids(self.angles, labels=titles, fontsize=14)

        for ax in self.axes[1:]:
            ax.patch.set_visible(False)
            ax.grid("off")
            ax.xaxis.set_visible(False)

        for ax, angle, label in zip(self.axes, self.angles, labels):
            ax.set_ylim(0, 0.5)
            ax.set_rgrids(label, angle=angle, labels=label)
            ax.spines["polar"].set_visible(False)

    def plot(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        return self.ax.plot(angle, values, *args, **kw)


def _group(rows, props):
    """
    Groups a list by properties

    :param rows: The list of rows to group
    :param props: The list of attributes names
    :return: The grouped list
    """
    data = np.array(list(sorted(map(lambda row: map(lambda p: getattr(row, p), props) + [repr(row)], rows))))
    return [(key, np.array(list(group))) for key, group in groupby(data, lambda r: r[0])]

def _plot_line(plt, f1_mean, label=u"Naïf", color=None):
    """
    Helper method to plot the Naif result constant line
    """

    if color is None:
        color = FlatColorsAdapter.from_string(label)

    line, = plt.plot([-10**5, 10**5], [f1_mean, f1_mean]),
    plt.setp(line, color=color, lw=2, aa=True, label=label)

def plotXY(groups, xscale_log=False, naif_value=0.093, axis=None, show_error=True,
           xlabel=None, ylabel=None, ploting=None, legend_adapter=None):
    """
    Plots a XY graph
    """
    plt.figure()

    if legend_adapter is None:
        legend_adapter = lambda s: s

    for key, group in groups:
        X = group[:, 1].astype(np.float64)
        Y = group[:, 2].astype(np.float64)

        line, = plt.plot(X, Y)
        plt.setp(line, color=FlatColorsAdapter.from_string(key), lw=2, aa=True, label=legend_adapter(key))

        if show_error:
            errors = group[:, 3].astype(np.float64)
            area = plt.fill_between(X, Y - errors, Y + errors)
            plt.setp(area, color=FlatColorsAdapter.from_string(key, 0.1), zorder=-1)

    if naif_value is not None:
        _plot_line(plt, naif_value, u"Naïf")

    if ploting is not None:
        ploting(plt)

    plt.legend(loc='best', prop={'size': 8})

    if xlabel:
        plt.xlabel(xlabel)

    if ylabel:
        plt.ylabel(ylabel)

    plt.axis(axis)
    plt.grid(True)

    if xscale_log:
        plt.xscale('log')

        # plt.show()

def _plot_radar_line(radar, labels, lines, value, label=u"Naïf"):
    """
    Helper method to plot the Naif result constant line on a Radar graph
    """

    line, = radar.plot([value for _ in labels], color=FlatColorsAdapter.from_string(label), lw=2, aa=True,
                           label=label)
    lines.append(line)


def plot_radar(groups, naif_value=0.093, axis=None, show_error=False, ploting=None,
               legend_loc=None, start_degrees=None,
               label_adapter=None, legend_adaper=None):
    """
    Plots a radar graph
    """

    if label_adapter is None:
        label_adapter = lambda s:s

    if legend_adaper is None:
        legend_adaper = lambda s:s

    if start_degrees is None:
        start_degrees = 45 if len(groups[0][1]) == 4 else 90

    if legend_loc is None:
        legend_loc = 'right' if len(groups[0][1]) == 4 and start_degrees == 45 else 'best'

    fig = plt.figure()
    labels = [label_adapter(s) for s in groups[0][1][:, 1]]
    steps = [[i / 10. for i in range(1, 5)]] * len(labels)
    radar = Radar(fig, titles=labels, labels=steps, start_degrees=start_degrees)

    lines = []

    for key, group in groups:
        values = group[:, 2].astype(np.float64)
        line, = radar.plot(values, "-", color=FlatColorsAdapter.from_string(key), lw=2, aa=True, label=legend_adaper(key))
        lines.append(line)

        if show_error:
            errors = group[:, 3].astype(np.float64)
            radar.plot(values - errors, color=FlatColorsAdapter.from_string(key, 0.2))
            radar.plot(values + errors, color=FlatColorsAdapter.from_string(key, 0.2))

    if naif_value is not None:
        _plot_radar_line(radar, labels, lines, naif_value, u"Naïf")

    if ploting is not None:
        ploting(radar, labels, lines)

    plt.legend(handles=lines, loc=legend_loc, prop={'size': 8})


def save_to_file(filename, factory):
    """
    Save a graph if it doesn't exist
    """

    filename = os.path.join('../results/graphs/', filename)

    print filename
    is_ignored = True
    if not os.path.isfile(filename) or save_to_file_force:
        factory()
        plt.savefig(filename, bbox_inches='tight')
        is_ignored = False
    print "\t", ("ignored" if is_ignored else "saved")

def clf_param(clf, param, lang=R.lang_fr, fs=R.fs_none, vect=R.vect_occurrence, xscale_log=True, x_axis=None, y_axis=None,
              naif_value=0.093):

    if not x_axis:
        x_axis = [0.00001, 10.]

    if not y_axis:
        y_axis = [0.0, 0.45]

    axis = x_axis + y_axis

    filtered = list(_filter(results, lang=lang, clf=clf, fs=fs, vect=vect))
    groups = _group(filtered, [R.doc, param, R.f1_mean, R.others_f1_std])
    plotXY(groups, xscale_log=xscale_log, axis=axis, xlabel=LabelAdapter.label_for_parameter(param), ylabel="F1-score",
           legend_adapter=LabelAdapter.label_for_doc, naif_value=naif_value)

def clf_param_by_fs(clf, param, n, lang=R.lang_fr, doc=R.doc_original, vect=R.vect_occurrence, xscale_log=True, x_axis=None, y_axis=None, naif_value=0.093):

    if not x_axis:
        x_axis = [0.00001, 10.]

    if not y_axis:
        y_axis = [0.0, 0.45]

    axis = x_axis + y_axis

    filtered = list(_filter(results, lang=lang, clf=clf, vect=vect, doc=doc, other=_or([
            _eq(R.fs_features_count, n),
            _eq(R.fs, R.fs_none),
    ])))
    groups = _group(filtered, [R.fs, param, R.f1_mean, R.others_f1_std])
    plotXY(groups, xscale_log=xscale_log, axis=axis, xlabel=LabelAdapter.label_for_parameter(param), ylabel="F1-score",
           legend_adapter=LabelAdapter.label_for_fs, naif_value=naif_value)

def clf_param_by_vect(clf, param, lang=R.lang_fr, doc=R.doc_original, fs=R.fs_none, xscale_log=True, x_axis=None, y_axis=None,
                      naif_value=0.093):

    if not x_axis:
        x_axis = [0.00001, 10.]

    if not y_axis:
        y_axis = [0.0, 0.45]

    axis = x_axis + y_axis

    filtered = list(_filter(results, lang=lang, clf=clf, doc=doc, fs=fs))
    groups = _group(filtered, [R.vect, param, R.f1_mean, R.others_f1_std])
    plotXY(groups, xscale_log=xscale_log, axis=axis, xlabel=LabelAdapter.label_for_parameter(param), ylabel="F1-score",
           legend_adapter=LabelAdapter.label_for_vect, naif_value=naif_value)


def svc_c_by_vect(naif_value=0.093):
    filtered = list(_filter(results, lang=R.lang_fr, clf=R.clf_linear_svm, doc=R.doc_original, fs=R.fs_none))
    groups = _group(filtered, [R.vect, R.clf_c, R.f1_mean, R.others_f1_std])
    plotXY(groups, xscale_log=True, axis=[0.00001, 10., 0.0, 0.45], xlabel=LabelAdapter.label_for_parameter(R.clf_c),
           ylabel="F1-score", legend_adapter=LabelAdapter.label_for_vect, naif_value=naif_value)


def clf_fs_by_doc_radar(clf, param, param_value, n, lang=R.lang_fr, vect=R.vect_occurrence, naif_value=0.093):
    filtered = list(_filter(results, lang=lang, clf=clf, vect=vect, other=_and([
        _eq(param, param_value),
        _or([
            _eq(R.fs_features_count, n),
            _eq(R.fs, R.fs_none),
        ]),
    ])))
    groups = _group(filtered, [R.fs, R.doc, R.f1_mean, R.others_f1_std])

    plot_normal = None

    # if should_plot_normal:
    #     normal = list(_filter(results, lang=R.lang_fr, clf=R.clf_linear_svm, vect=R.vect_occurrence, doc=R.doc_original, other=_and([
    #         _eq(R.clf_c, c),
    #         _eq(R.fs, R.fs_none)
    #     ])))
    #     assert len(normal) == 1
    #     normal_f1_mean = getattr(normal[0], R.f1_mean)
    #     plot_normal = lambda radar, labels, lines:_plot_radar_line(radar, labels, lines, normal_f1_mean, "none")

    plot_radar(groups, ploting=plot_normal, label_adapter=LabelAdapter.label_for_doc, legend_adaper=LabelAdapter.label_for_fs, naif_value=naif_value)

def svc_fs_by_vect_radar(c, n, should_plot_normal=False, lang=R.lang_fr, naif_value=0.093):
    filtered = list(_filter(results, lang=lang, clf=R.clf_linear_svm, doc=R.doc_original, other=_and([
        _eq(R.clf_c, c),
        _or([
            _eq(R.fs_features_count, n),
            _eq(R.fs, R.fs_none),
        ]),
    ])))
    groups = _group(filtered, [R.fs, R.vect, R.f1_mean, R.others_f1_std])

    plot_normal = None

    if should_plot_normal:
        normal = list(_filter(results, lang=lang, clf=R.clf_linear_svm, vect=R.vect_occurrence, doc=R.doc_original, other=_and([
            _eq(R.clf_c, c),
            _eq(R.fs, R.fs_none)
        ])))
        assert len(normal) == 1
        normal_f1_mean = getattr(normal[0], R.f1_mean)
        plot_normal = lambda radar, labels, lines:_plot_radar_line(radar, labels, lines, normal_f1_mean, LabelAdapter.label_for_fs(R.fs_none))

    plot_radar(groups, ploting=plot_normal, label_adapter=LabelAdapter.label_for_vect, legend_adaper=LabelAdapter.label_for_fs, naif_value=naif_value)

def fs(vect=R.vect_occurrence, c=0.001, lang=R.lang_fr, naif_value=0.093):

    normal = list(_filter(results, lang=lang, clf=R.clf_linear_svm, vect=vect, doc=R.doc_original, other=_and([
        _eq(R.clf_c, c),
        _eq(R.fs, R.fs_none)
    ])))
    assert len(normal) == 1
    normal_f1_mean = getattr(normal[0], R.f1_mean)
    plot_normal = lambda plt:_plot_line(plt, normal_f1_mean, LabelAdapter.label_for_fs(R.fs_none), FlatColorsAdapter.from_string(R.fs_none))

    filtered = list(_filter(results, lang=lang, clf=R.clf_linear_svm, vect=vect, doc=R.doc_original, other=_and([
        _eq(R.clf_c, c),
        _neq(R.fs, R.fs_none),
    ])))
    groups = _group(filtered, [R.fs, R.fs_features_count, R.f1_mean, R.others_f1_std])
    plotXY(groups, xscale_log=True, axis=[10, 5000, 0.0, 0.45],
           xlabel=LabelAdapter.label_for_parameter(R.fs_n_components),
           ylabel="F1-score", ploting=plot_normal,
           legend_adapter=LabelAdapter.label_for_fs,
           naif_value=naif_value)

save_to_file_force = True

save_to_file("svc_c_by_document.pdf", lambda:clf_param(R.clf_linear_svm, R.clf_c, x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 1000, x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_100.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 100, doc=R.doc_lemmatized_filtered, x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_200.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 200, doc=R.doc_lemmatized_filtered, x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_500.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 500, doc=R.doc_lemmatized_filtered, x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_1000.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 1000, doc=R.doc_lemmatized_filtered,  x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_2000.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 2000, doc=R.doc_lemmatized_filtered,  x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_fs_lemmaf_5000.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 5000, doc=R.doc_lemmatized_filtered,  x_axis=[0.00001, 10.]))
save_to_file("svc_c_by_vect.pdf", lambda:clf_param_by_vect(R.clf_linear_svm, R.clf_c, x_axis=[0.00001, 10.]))

save_to_file("svc_c_by_c__count_vect.pdf", lambda:clf_param_by_vect(R.clf_linear_svm, R.clf_c, x_axis=[0.00001, 10.]))

save_to_file("svc_fs_by_doc_001_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.01, n=1000))
save_to_file("svc_fs_by_doc_001_100.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.01, n=100))
save_to_file("svc_fs_by_doc_0001_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.001, n=1000))
save_to_file("svc_fs_by_vect_001_1000.pdf", lambda:svc_fs_by_vect_radar(c=0.01, n=1000))
save_to_file("svc_fs_by_vect_001_100.pdf", lambda:svc_fs_by_vect_radar(c=0.01, n=100))
save_to_file("svc_fs_by_vect_0001_1000.pdf", lambda:svc_fs_by_vect_radar(c=0.001, n=1000))

save_to_file("bnb_a_by_document.pdf", lambda:clf_param(R.clf_bernoulliNB, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("bnb_a_by_fs_100.pdf", lambda:clf_param_by_fs(R.clf_bernoulliNB, R.clf_alpha, 100, x_axis=[0.00001, 1.]))
save_to_file("bnb_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_bernoulliNB, R.clf_alpha, 1000, x_axis=[0.00001, 1.]))
save_to_file("bnb_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_bernoulliNB, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("bnb_fs_by_doc_01_100.pdf", lambda:clf_fs_by_doc_radar(R.clf_bernoulliNB, R.clf_alpha, 0.1, n=100))

save_to_file("mnb_a_by_document.pdf", lambda:clf_param(R.clf_multinomialNB, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("mnb_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_multinomialNB, R.clf_alpha, 1000, x_axis=[0.00001, 1.]))
save_to_file("mnb_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_multinomialNB, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("mnb_fs_by_doc_01_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_multinomialNB, R.clf_alpha, 0.1, n=1000))

save_to_file("sgd_a_by_document.pdf", lambda:clf_param(R.clf_sgd, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("sgd_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_sgd, R.clf_alpha, 1000, x_axis=[0.00001, 1.]))
save_to_file("sgd_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_sgd, R.clf_alpha, x_axis=[0.00001, 1.]))
save_to_file("sgd_fs_by_doc_01_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_sgd, R.clf_alpha, 0.1, n=1000))



save_to_file("fs_count_001.pdf", lambda:fs(vect=R.vect_occurrence, c=0.01))
save_to_file("fs_count_0001.pdf", lambda:fs(vect=R.vect_occurrence, c=0.001))
save_to_file("fs_tfidf_001.pdf", lambda:fs(vect=R.vect_tfidf, c=0.01))

######################## EN #######################################################################

save_to_file("en_svc_c_by_document.pdf", lambda:clf_param(R.clf_linear_svm, R.clf_c, lang=R.lang_en, x_axis=[0.00001, 10.], naif_value=0.048))
save_to_file("en_svc_c_by_fs.pdf", lambda:clf_param_by_fs(R.clf_linear_svm, R.clf_c, 1000, lang=R.lang_en, x_axis=[0.00001, 10.], naif_value=0.048))
save_to_file("en_svc_c_by_vect.pdf", lambda:clf_param_by_vect(R.clf_linear_svm, R.clf_c, lang=R.lang_en, x_axis=[0.00001, 10.], naif_value=0.048))

save_to_file("en_svc_c_by_c__count_vect.pdf", lambda:clf_param_by_vect(R.clf_linear_svm, R.clf_c, lang=R.lang_en, x_axis=[0.00001, 10.], naif_value=0.048))

save_to_file("en_svc_fs_by_doc_001_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.01, lang=R.lang_en, n=1000, naif_value=0.048))
save_to_file("en_svc_fs_by_doc_001_100.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.01, lang=R.lang_en, n=100, naif_value=0.048))
save_to_file("en_svc_fs_by_doc_0001_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_linear_svm, R.clf_c, 0.001, lang=R.lang_en, n=1000, naif_value=0.048))
save_to_file("en_svc_fs_by_vect_001_1000.pdf", lambda:svc_fs_by_vect_radar(lang=R.lang_en, c=0.01, n=1000))
save_to_file("en_svc_fs_by_vect_001_100.pdf", lambda:svc_fs_by_vect_radar(lang=R.lang_en, c=0.01, n=100))
save_to_file("en_svc_fs_by_vect_0001_1000.pdf", lambda:svc_fs_by_vect_radar(lang=R.lang_en, c=0.001, n=1000))

save_to_file("en_bnb_a_by_document.pdf", lambda:clf_param(R.clf_bernoulliNB, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_bnb_a_by_fs_100.pdf", lambda:clf_param_by_fs(R.clf_bernoulliNB, R.clf_alpha, 100, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_bnb_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_bernoulliNB, R.clf_alpha, 1000, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_bnb_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_bernoulliNB, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_bnb_fs_by_doc_01_100.pdf", lambda:clf_fs_by_doc_radar(R.clf_bernoulliNB, R.clf_alpha, 0.1, lang=R.lang_en, n=100, naif_value=0.048))

save_to_file("en_mnb_a_by_document.pdf", lambda:clf_param(R.clf_multinomialNB, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_mnb_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_multinomialNB, R.clf_alpha, 1000, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_mnb_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_multinomialNB, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_mnb_fs_by_doc_01_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_multinomialNB, R.clf_alpha, 0.1, lang=R.lang_en, n=1000, naif_value=0.048))

save_to_file("en_sgd_a_by_document.pdf", lambda:clf_param(R.clf_sgd, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_sgd_a_by_fs.pdf", lambda:clf_param_by_fs(R.clf_sgd, R.clf_alpha, 1000, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_sgd_a_by_vect.pdf", lambda:clf_param_by_vect(R.clf_sgd, R.clf_alpha, lang=R.lang_en, x_axis=[0.00001, 1.], naif_value=0.048))
save_to_file("en_sgd_fs_by_doc_01_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_sgd, R.clf_alpha, 0.1, lang=R.lang_en, n=1000, naif_value=0.048))
save_to_file("en_sgd_fs_by_doc_0001_1000.pdf", lambda:clf_fs_by_doc_radar(R.clf_sgd, R.clf_alpha, 0.001, lang=R.lang_en, n=1000, naif_value=0.048))

save_to_file("en_fs_count_001.pdf", lambda:fs(vect=R.vect_occurrence, lang=R.lang_en, c=0.01, naif_value=0.048))
save_to_file("en_fs_count_0001.pdf", lambda:fs(vect=R.vect_occurrence, lang=R.lang_en, c=0.001, naif_value=0.048))
save_to_file("en_fs_tfidf_001.pdf", lambda:fs(vect=R.vect_tfidf, lang=R.lang_en, c=0.01, naif_value=0.048))
