# coding=utf-8
from collections import Counter
from itertools import groupby
import os
from sklearn.decomposition import TruncatedSVD
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import SGDClassifier, Perceptron, PassiveAggressiveClassifier, Ridge
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import NearestCentroid, KNeighborsClassifier
from sklearn.svm import LinearSVC
from sklearn.svm.classes import SVC
from src.classifier_3cgp.feature_selection.bi_normal_separation import bns
from src.classifier_3cgp.feature_selection.cube_mutual_information import mi3
from src.classifier_3cgp.feature_selection.mutual_information import mi

from src.classifier_3cgp.importers.joblib_importer import JoblibImporter
from src.classifier_3cgp.test_generator import TestGenerator

# SVM (Support vector machines) :       http://scikit-learn.org/stable/modules/svm.html
# SGD (Stochastic Gradient Descent) :   http://scikit-learn.org/stable/modules/sgd.html
# NN  (Nearest Neighbors) :             http://scikit-learn.org/stable/modules/neighbors.html
# NB  (Naive Bayes) :                   http://scikit-learn.org/stable/modules/naive_bayes.html
# DT  (Decision Tree) :                 http://scikit-learn.org/stable/modules/tree.html
# RF  (Random Forest) :                 http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html


__author__ = 'Sébastien Rigaux'

import_dir = "../data/documents/"

#region --- Imports the corpus ------------------------------------------------
# Deserialize the corpus data serialized by the 'corpus_cacher.py' program

french_corpus = JoblibImporter(import_dir + "french.bin").import_corpus()
french_stemmed_corpus = JoblibImporter(import_dir + "french_stemmed.bin").import_corpus()
french_lemmatized_corpus = JoblibImporter(import_dir + "french_lemmatized.bin").import_corpus()
french_filtered_lemmatized_corpus = JoblibImporter(import_dir + "french_filtered_lemmatized.bin").import_corpus()

english_corpus = JoblibImporter(import_dir + "english.bin").import_corpus()
english_stemmed_corpus = JoblibImporter(import_dir + "english_stemmed.bin").import_corpus()
english_lemmatized_corpus = JoblibImporter(import_dir + "english_lemmatized.bin").import_corpus()
english_filtered_lemmatized_corpus = JoblibImporter(import_dir + "english_filtered_lemmatized.bin").import_corpus()

#endregion

#region --- Corpus ------------------------------------------------------------

french_corpus_dict = {
    "": french_corpus,
    "stemmed": french_stemmed_corpus,
    "lemmatized": french_lemmatized_corpus,
    "lemmatized filtered": french_filtered_lemmatized_corpus,
}

english_corpus_dict = {
    "": english_corpus,
    # "stemmed": english_stemmed_corpus,
    # "lemmatized": english_lemmatized_corpus,
    "lemmatized filtered": english_filtered_lemmatized_corpus
}

corpus_dict_by_language = {
    # "fr": french_corpus,
    "en": english_corpus_dict
}

#endregion

#region --- Vectorizers -------------------------------------------------------
vectorizers = {
    "count": (CountVectorizer(), dict(
        vect__max_df=[1.0],
        vect__min_df=[1],
        vect__max_features=[None],
        vect__ngram_range=[(1, 1)],
        vect__stop_words=['english']
    )),
    # "binnary": (CountVectorizer(binary=True), dict(
    #     vect__max_df=[1.0],
    #     vect__min_df=[1],
    #     vect__max_features=[None],
    #     vect__ngram_range=[(1, 1)],
    #     vect__stop_words=['english']
    # )),
    # "Tf": (TfidfVectorizer(use_idf=False), dict(
    #     vect__max_df=[1.0],
    #     vect__min_df=[1],
    #     vect__max_features=[None],
    #     vect__ngram_range=[(1, 1)],
    #     vect__norm=['l2'],
    #     vect__stop_words=['english']
    # )),
    "Tf-Idf": (TfidfVectorizer(), dict(
        vect__max_df=[1.0],
        vect__min_df=[1],
        vect__max_features=[None],
        vect__ngram_range=[(1, 1)],
        vect__sublinear_tf=[True],
        vect__norm=['l2'],
        vect__stop_words=['english']
    ))
}
#endregion

#region --- Feature Identification --------------------------------------------
k = [
    # 10,
    # 20,
    # 50,
    # 100,
    # 200,
    # 500,
    1000,
    # 1500,
    # 2000,
    # 5000
]

fs_params = dict(
    fs__k=k,
)

feature_identification_methods = {
    "none": None,
    "chi2": (SelectKBest(chi2), fs_params),
    "mi_2": (SelectKBest(mi), fs_params),
    "mi3_2": (SelectKBest(mi3), fs_params),
    "bnf": (SelectKBest(bns), fs_params),
    "TruncatedSVD": (TruncatedSVD(), dict(
        fs__n_components=k,
    ))
}
#endregion

#region --- Classifiers -------------------------------------------------------
classifiers = {
    "svc": (OneVsRestClassifier(LinearSVC(), n_jobs=-1),
            dict(
                clf__estimator__C=[
                    # 0.000010,
                    # 0.00010,
                    # 0.00050,
                    # # 0.00075,
                    0.0010,
                    # 0.0025,
                    # # 0.0035,
                    # 0.0050,
                    0.010,
                    # 0.050,
                    # 0.10,
                    # 0.50,
                    # 1,
                    # 5,
                    # 10
                ],
                clf__estimator__penalty=[
                    'l2'
                ],
                clf__estimator__loss=[
                    'l2'
                ],
                clf__estimator__class_weight=[
                    'auto'
                ],
                clf__estimator__dual=[
                    True
                ]
            )),
    # "BernoulliNB": (OneVsRestClassifier(BernoulliNB()),
    #                 dict(
    #                     clf__estimator__alpha=[
    #                         10,
    #                         1.000000,
    #                         0.500000,
    #                         0.250000,
    #                         0.125000,
    #                         0.100000,
    #                         0.050000,
    #                         0.025000,
    #                         0.010000,
    #                         0.001000,
    #                         0.000100,
    #                         0.000010,
    #                         0.000001
    #                     ]
    #                 )),
    # "svc-kernel": (OneVsRestClassifier(SVC(probability=True), n_jobs=-1),
    #                 dict(
    #                     clf__estimator__kernel=[
    #                         'rbf',
    #                         # 'poly',
    #                         # 'sigmoid'
    #                     ],
    #                     clf__estimator__C=[
    #                         # 10,
    #                         1.0,
    #                         # 0.5,
    #                         # 0.1,
    #                         # 0.05,
    #                         # 0.01,
    #                         # 0.001,
    #                         # 0.0001,
    #                         # 0.00001,
    #                         # 0.000001
    #                     ],
    #                     clf__estimator__degree=[
    #                         2,
    #                         # 3
    #                     ],
    #                     clf__estimator__coef0=[
    #                         0.0
    #                     ],
    #                     clf__estimator__gamma=[
    #                         0.0,
    #                         0.1,
    #                         1.0,
    #                         # 2.0
    #                     ],
    #                     clf__estimator__class_weight=[
    #                         'auto'
    #                     ]
    #                 )),
    # "MultinomialNB": (OneVsRestClassifier(MultinomialNB(), n_jobs=-1),
    #                   dict(
    #                       clf__estimator__alpha=[
    #                         10,
    #                         1.000000,
    #                         0.500000,
    #                         0.250000,
    #                         0.200000,
    #                         0.125000,
    #                         0.100000,
    #                         0.050000,
    #                         0.010000,
    #                         0.001000,
    #                         0.000100,
    #                         0.000010,
    #                         0.000001
    #                     ]
    #                   )),
    "SGD": (OneVsRestClassifier(SGDClassifier(), n_jobs=-1),
            dict(
                clf__estimator__loss=(
                    'log',
                    # 'hinge'
                ),
                clf__estimator__penalty=[
                    # 'l1',
                    'l2',
                    # 'elasticnet'
                ],
                clf__estimator__alpha=[
                    # 10,
                    # 1.0,
                    # 0.5,
                    0.1,
                    # 0.05,
                    # 0.025,
                    0.01,
                    0.001,
                    # 0.0001,
                    # 0.00001,
                    # 0.000001
                ],
                clf__estimator__class_weight=[
                    'auto'
                ]
            )),
    # "PassiveAggressive": (OneVsRestClassifier(PassiveAggressiveClassifier(), n_jobs=-1),
    #         dict(
    #             clf__estimator__C=[
    #                 1.0,
    #                 0.1,
    #                 0.01,
    #                 0.001,
    #                 0.0001,
    #                 0.00001,
    #                 0.000001
    #             ],
    #         )),
    # "Perceptron": (OneVsRestClassifier(Perceptron(), n_jobs=-1),
    #         dict(
    #             clf__estimator__penalty=[
    #                 None,
    #                 # 'l1',
    #                 #'l2',
    #                 # 'elasticnet'
    #             ],
    #             clf__estimator__alpha=[
    #                 # 1.0,
    #                 # 0.1,
    #                 #0.01,
    #                 0.001,
    #                 0.0001,
    #                 0.00001,
    #                 # 0.000001
    #             ],
    #             clf__estimator__eta0=[
    #                 1.0,
    #                 0.1,
    #                 0.01,
    #                 0.001,
    #                 0.0001,
    #                 0.00001,
    #                 0.000001
    #             ],
    #             clf__estimator__class_weight=[
    #                 'auto'
    #             ]
    #         )),
    # "Ridge": (OneVsRestClassifier(Ridge(), n_jobs=-1),
    #         dict(
    #             clf__estimator__alpha=[
    #                 # 1.0,
    #                 # 0.1,
    #                 # 0.01,
    #                 0.001,
    #                 # 0.0001,
    #                 # 0.00001,
    #                 # 0.000001
    #             ],
    #             clf__estimator__tol=[
    #                 # 1.0,
    #                 # 0.1,
    #                 0.01,
    #                 0.001,
    #                 0.0001,
    #                 # 0.00001,
    #                 # 0.000001
    #             ],
    #         )),
    # "KNeighborsClassifier": (OneVsRestClassifier(KNeighborsClassifier(), n_jobs=-1),
    #         dict(
    #             clf__estimator__n_neighbors=[
    #                 5,
    #                 15
    #             ],
    #             clf__estimator__p=[
    #                 2
    #             ],
    #             clf__estimator__leaf_size=[
    #                 30
    #             ],
    #             clf__estimator__weights=[
    #                 'uniform'
    #             ]
    #         )),
    # "AdaBoost": (OneVsRestClassifier(AdaBoostClassifier(), n_jobs=-1),
    #         dict(
    #             clf__estimator__n_estimators=[
    #                 25,
    #                 50,
    #                 100,
    #             ],
    #             clf__estimator__learning_rate=[
    #                 1.
    #             ]
    #         )),
    # "Dummy-Stratified": (OneVsRestClassifier(DummyClassifier(strategy='stratified', random_state=0), n_jobs=1), dict()),
    # "Dummy-MostFrequent": (
    #     OneVsRestClassifier(DummyClassifier(strategy='most_frequent', random_state=0), n_jobs=1), dict())
}
#endregion

import csv


def load_csv(filename):
    """
    Load the csv file containing the results

    :param filename: the path to the file
    :type filename: str

    :return: The results (a dictionary of dict results identify by its hash)
    :rtype: dict[str, dict[str, str]]
    """
    results = {}
    if os.path.isfile(filename):
        with open(filename) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                h = row["hash"]

                if h in results:
                    l = results[h]
                    l.append(row)
                else:
                    results[row["hash"]] = [row]

    return results

# loads the results
filename = '../results/results.csv'
results = load_csv(filename)

# saves the csv column names
keys = set() if not results.values() else set(results.values()[0][0].keys())

# for each language
for lang, corpus_dict in corpus_dict_by_language.items():

    # for each corpus adaptation
    for corpus in corpus_dict.items():

        # for each vectorizer method
        for vectorizer in vectorizers.items():

            # for each feature identification method
            for fi_method in feature_identification_methods.items():

                # for each classifier ML method
                for classifier in classifiers.items():

                    # create a generator for each test depending on each parameters combination
                    t = TestGenerator(lang, corpus, vectorizer, fi_method, classifier)

                    for result, hash in t.generate(lambda h: str(h) in results):

                        # Checks if there is no additional csv columns to add
                        if not keys.issuperset(result.keys()):

                            # if there is additional csv columns
                            # rewrite the file with new columns

                            keys.update(result.keys())
                            with open(filename, 'wb') as output_file:
                                dict_writer = csv.DictWriter(output_file, keys)
                                dict_writer.writeheader()
                                dict_writer.writerows(results.values())

                        # Appends the result csv row to the output_file

                        with open(filename, 'a') as output_file:
                                dict_writer = csv.DictWriter(output_file, keys)
                                dict_writer.writerow(result)

                        results[hash] = result
