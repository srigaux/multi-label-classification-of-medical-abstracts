#!/usr/bin/env python
# coding=utf-8
import argparse
from src.classifier_3cgp.classifiers.english_classifier import EnglishClassifier
from src.classifier_3cgp.classifiers.french_classifier import FrenchClassifier

__author__ = 'Sébastien Rigaux'

parser = argparse.ArgumentParser()

clf_group = parser.add_mutually_exclusive_group(required=True)
clf_group.add_argument("-fr", "--french", action="store_true", help="French classifier")
clf_group.add_argument("-en", "--english", action="store_true", help="English classifier")

doc_group = parser.add_mutually_exclusive_group(required=True)
doc_group.add_argument("text", nargs='?', help="The text to classify")
doc_group.add_argument("-f","--file", help="A file to classify")

args = parser.parse_args()

if args.french:
    clf = FrenchClassifier()

elif args.english:
    clf = EnglishClassifier()


if args.text is not None:
    text = args.text

elif args.file is not None:
    with open(args.file, 'r') as content_file:
        text = content_file.read()

print clf.predict(text)