#Programs

##predict.py

Classifies a text passed as parameter 

```
usage: predict.py [-h] (-fr | -en) [-f FILE] [text]

positional arguments:
  text                  The text to classify

optional arguments:
  -h, --help            show this help message and exit
  -fr, --french         French classifier
  -en, --english        English classifier
  -f FILE, --file FILE  A file to classify

```

##test_generator.py
Generates a set of classifiers and their tests, and saves the results in a csv file

##corpus_cacher.py
Adapts corpus and saves the suitable corpus in binary files

##test_{lang}.py
Creates a series of classifiers and saves detailed test results in JSON files.
We can read the results using the LogViewer.

##graphs*.py
Classes to generate report charts
