# coding=utf-8
import re

from bs4 import BeautifulSoup

from src.classifier_3cgp.adapters.lemmatizer_corpus_adapter import LemmatizerCorpusAdapter, TreeTaggerLemmatizer, \
    WordNetLemmatizer
from src.classifier_3cgp.adapters.mutli_document_aggregator import MultiCorpusAggregator
from src.classifier_3cgp.adapters.pipeline_corpus_adapter import PipelineCorpusAdapter
from src.classifier_3cgp.adapters.regex_tag_document_adapter import RegexTagCorpusAdapter
from src.classifier_3cgp.adapters.stemmer_corpus_adapter import FrenchStemmerCorpusAdapter, EnglishStemmerCorpusAdapter
from src.classifier_3cgp.adapters.text_corpus_adapter import TextCorpusAdapter
from src.classifier_3cgp.exporters.joblib_exporter import JoblibExporter
from src.classifier_3cgp.importers.atlas_ti.stored_hu_xml_importer import StoredHUXmlImporter
from src.classifier_3cgp.importers.wonca.wonca_importer import WoncaImporter

__author__ = 'Sébastien Rigaux'

# --- Directories -------------------------------------------

atlas_ti_folder = "../data/atlas_ti/"
wonca_paris_2007_folder = "../data/wonca/paris_2007/"

# --- Importers ---------------------------------------------

clermont_doc = StoredHUXmlImporter(atlas_ti_folder + "Clermont.xml").import_corpus()
lille_doc = StoredHUXmlImporter(atlas_ti_folder + "Lille.xml").import_corpus()

wonca_paris_2007_doc = WoncaImporter(wonca_paris_2007_folder + "annotations.csv",
                                     wonca_paris_2007_folder + "xml_abstract").import_corpus()

# --- Aggregator --------------------------------------------

french_corpus = MultiCorpusAggregator().aggregate_documents([clermont_doc, lille_doc])
english_corpus = wonca_paris_2007_doc

# --- Adapters ----------------------------------------------

# Removes excessive white spaces
html_white_space_regex = re.compile(r"(\s+)", re.MULTILINE)

# Strips html tags
strip_html_text_adapter = TextCorpusAdapter(
    lambda s: re.sub(html_white_space_regex, " ", BeautifulSoup(s).get_text(strip=True)))

# Extracts code from categories name
qcodes_tags_filter_adapter = RegexTagCorpusAdapter(r"Q[CDEHOPRST]\d{1,3}")  # "Q[CDPRST].*" | "Q[CDEHPR]\d+.*"

# Concatenates the two adapters
default_document_adapter = PipelineCorpusAdapter([strip_html_text_adapter,
                                                  qcodes_tags_filter_adapter])

# Adapts the corpus
french_corpus = default_document_adapter.adapt_corpus(french_corpus)
english_corpus = default_document_adapter.adapt_corpus(english_corpus)


only_char_regex = re.compile("^[a-zA-ZÀ-ÿ]*$")

# French corpus factories

french_corpus_factory = \
    lambda: french_corpus

french_stemmed_corpus_factory = \
    lambda: FrenchStemmerCorpusAdapter().adapt_corpus(french_corpus)

french_lemmatized_corpus_factory = \
    lambda: LemmatizerCorpusAdapter().adapt_corpus(french_corpus)

french_filtered_lemmatized_corpus_factory = \
    lambda: LemmatizerCorpusAdapter(
        TreeTaggerLemmatizer(word_filter=lambda w: w.postag.startswith(('A', 'NAM', 'NOM', 'VER')))
    ).adapt_corpus(french_corpus)

# English corpus factories

english_corpus_factory = \
    lambda: english_corpus

english_stemmed_corpus_factory = \
    lambda: EnglishStemmerCorpusAdapter().adapt_corpus(english_corpus)

english_lemmatized_corpus_factory = \
    lambda: LemmatizerCorpusAdapter(WordNetLemmatizer()).adapt_corpus(english_corpus)

english_filtered_lemmatized_corpus_factory = \
    lambda: LemmatizerCorpusAdapter(
        WordNetLemmatizer(
            word_filter=lambda w: (only_char_regex.match(w.word)
                                   and
                                   w.postag.startswith(('FW', 'J', 'MD', 'N', 'R', 'V', 'W')))
        )
    ).adapt_corpus(english_corpus)


# --- Exporters ---------------------------------------------


exporter = JoblibExporter()

export_dir = "../data/documents/"

exporter.export(french_corpus_factory, export_dir + "french.bin")
exporter.export(french_stemmed_corpus_factory, export_dir + "french_stemmed.bin")
exporter.export(french_lemmatized_corpus_factory, export_dir + "french_lemmatized.bin")
exporter.export(french_filtered_lemmatized_corpus_factory, export_dir + "french_filtered_lemmatized.bin")

exporter.export(english_corpus_factory, export_dir + "english.bin")
exporter.export(english_stemmed_corpus_factory, export_dir + "english_stemmed.bin")
exporter.export(english_lemmatized_corpus_factory, export_dir + "english_lemmatized.bin")
exporter.export(english_filtered_lemmatized_corpus_factory, export_dir + "english_filtered_lemmatized.bin")
