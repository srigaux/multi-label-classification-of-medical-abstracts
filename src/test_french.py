# coding=utf-8
import re
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model.stochastic_gradient import SGDClassifier
from sklearn.multiclass import OneVsRestClassifier, OutputCodeClassifier
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.neighbors.unsupervised import NearestNeighbors
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.svm.classes import SVC
from sklearn.tree import DecisionTreeClassifier

from src.classifier_3cgp.adapters.tag_text_adapter_filer_corpus_adapter import TagTextAdapterFilterCorpusAdapter
from src.classifier_3cgp.adapters.lemmatizer_corpus_adapter import TreeTaggerLemmatizer, \
    LemmatizerCorpusAdapter
from src.classifier_3cgp.adapters.stemmer_corpus_adapter import FrenchStemmerCorpusAdapter
from src.classifier_3cgp.feature_selection.bi_normal_separation import bns
from src.classifier_3cgp.feature_selection.cube_mutual_information import mi3
from src.classifier_3cgp.feature_selection.mutual_information import mi
from src.classifier_3cgp.importers.atlas_ti.adapters.stored_hu_corpus_adapter import PerDocumentStoredHUCorpusAdapter, \
    PerQuotationStoredHUCorpusAdapter, OnlyCodedQuotationPerDocumentHUDocumentAdapter
from src.classifier_3cgp.tester_french import Tester


# SVM (Support vector machines) :       http://scikit-learn.org/stable/modules/svm.html
# SGD (Stochastic Gradient Descent) :   http://scikit-learn.org/stable/modules/sgd.html
# NN  (Nearest Neighbors) :             http://scikit-learn.org/stable/modules/neighbors.html
# NB  (Naive Bayes) :                   http://scikit-learn.org/stable/modules/naive_bayes.html
# DT  (Decision Tree) :                 http://scikit-learn.org/stable/modules/tree.html
# RF  (Random Forest) :                 http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
from src.classifier_3cgp.utils.dense_transformer import DenseTransformer

__author__ = 'Sébastien Rigaux'

#region --- Corpus Adapters ---------------------------------------------------
tag_filter_regex = re.compile("Q.*")

per_document_stored_hu_adapter =                        PerDocumentStoredHUCorpusAdapter()
per_quotation_stored_hu_adapter =                       PerQuotationStoredHUCorpusAdapter()
per_document_only_coded_quotations_stored_hu_adapter =  OnlyCodedQuotationPerDocumentHUDocumentAdapter()

default_corpus_adapter =          TagTextAdapterFilterCorpusAdapter(tag_filter=lambda s: tag_filter_regex.match(s))
only_2alpha_tag_corpus_adapter =  TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s[:2])
only_3alpha_tag_corpus_adapter =  TagTextAdapterFilterCorpusAdapter(tag_adapter=lambda s: s[:3])

stemmer_corpus_adapter =          FrenchStemmerCorpusAdapter()
lemmatizer_corpus_adapter =       LemmatizerCorpusAdapter()

lemmatizer_filtered_corpus_adapter = LemmatizerCorpusAdapter(TreeTaggerLemmatizer(
    word_filter=lambda w: w.postag.startswith(('A', 'INT', 'KON', 'N', 'PRP', 'PUN', 'VER'))))
lemmatizer_filtered_corpus_adapter_2 = LemmatizerCorpusAdapter(TreeTaggerLemmatizer(
    word_filter=lambda w: w.postag.startswith(('A', 'NAM', 'NOM', 'VER'))))

# word_filter=lambda w: w.postag.startswith(('A', 'KON', 'N', 'VER'))))
# http://www.ims.uni-stuttgart.de/institut/mitarbeiter/schmid/french-tagset.html

french_stop_words = Tester.stop_words()

#endregion

#region --- Vectorizers -------------------------------------------------------
count_vectorizer =              ('vectorizer', CountVectorizer(stop_words=french_stop_words))
tfidf_vectorizer_1gram =        ('vectorizer', TfidfVectorizer(ngram_range=(1, 1), stop_words=french_stop_words))
tfidf_vectorizer_2gram =        ('vectorizer', TfidfVectorizer(ngram_range=(1, 2), stop_words=french_stop_words))
tfidf_vectorizer_3gram =        ('vectorizer', TfidfVectorizer(ngram_range=(1, 3), stop_words=french_stop_words))
tfidf_vectorizer_2_3gram =      ('vectorizer', TfidfVectorizer(ngram_range=(2, 3), stop_words=french_stop_words))
tfidf_vectorizer_max_df_05 = (
    'vectorizer', TfidfVectorizer(stop_words=french_stop_words,
                                  max_df=0.4,
                                  sublinear_tf=True,
                                  max_features=700))
#endregion

#region --- Others ------------------------------------------------------------
to_dense = ('to_dense', DenseTransformer())

#endregion

#region --- Classifiers -------------------------------------------------------
# ovr_l_svc =               ('clf', OneVsRestClassifier(SVC(kernel='linear', probability=True), n_jobs=1))
ovr_l_svc =                 ('clf', OneVsRestClassifier(LinearSVC(), n_jobs=-1))
ovr_l_svc_w_auto =          ('clf', OneVsRestClassifier(LinearSVC(class_weight='auto'), n_jobs=-1))
ovr_l_svc_w_auto_c_00009 =  ('clf', OneVsRestClassifier(LinearSVC(class_weight='auto', C=0.0009), n_jobs=-1))
ovr_l_svc_w_auto_c_002 =    ('clf', OneVsRestClassifier(LinearSVC(class_weight='auto', C=0.02), n_jobs=-1))

ovr_svc =                   ('clf', OneVsRestClassifier(SVC(probability=True), n_jobs=-1))
ovr_svc_kernel_rbf =        ('clf', OneVsRestClassifier(SVC(), n_jobs=-1))
ovr_nn =                    ('clf', OneVsRestClassifier(NearestNeighbors(n_neighbors=2, algorithm='ball_tree'), n_jobs=-1))
# ovo_l_svc =               ('clf', OneVsOneClassifier(LinearSVC(), n_jobs=1))

ovr_g_nb =                  ('clf', OneVsRestClassifier(GaussianNB(), n_jobs=-1))
ovr_m_nb =                  ('clf', OneVsRestClassifier(MultinomialNB(alpha=0.01), n_jobs=-1))
ovr_b_nb =                  ('clf', OneVsRestClassifier(BernoulliNB(), n_jobs=-1))
ovr_sgd =                   ('clf', OneVsRestClassifier(SGDClassifier(), n_jobs=-1))
ovr_sgd_w_auto =            ('clf', OneVsRestClassifier(SGDClassifier(class_weight='auto'), n_jobs=-1))
ovr_dt =                    ('clf', OneVsRestClassifier(DecisionTreeClassifier()))  # pas les sparse
ovr_rf =                    ('clf', OneVsRestClassifier(RandomForestClassifier()))
ovr_ab =                    ('clf', OneVsRestClassifier(AdaBoostClassifier(DecisionTreeClassifier(max_depth=2),
                                                                           n_estimators=10,
                                                                           learning_rate=1.5)))
ovr_best =                  ('clf', OneVsRestClassifier(SGDClassifier(loss='log', penalty='l2', class_weight='auto', alpha=0.1), n_jobs=-1))

occ_l_svc =                 ('clf', OutputCodeClassifier(LinearSVC(random_state=0), code_size=2, random_state=0, n_jobs=-1))

#endregion

#region --- Feature Identification --------------------------------------------
fi_chi2_100  = ('fi', SelectKBest(chi2, k=100))
fi_chi2_1000 = ('fi', SelectKBest(chi2, k=1000))

fi_bns_100   = ('fi', SelectKBest(bns, k=100))
fi_bns_1000  = ('fi', SelectKBest(bns, k=1000))

fi_mi_100  =   ('fi', SelectKBest(mi, k=100))
fi_mi_1000 =   ('fs', SelectKBest(mi, k=1000))

fi_mi3_100   = ('fi', SelectKBest(mi3, k=100))
fi_mi3_1000  = ('fi', SelectKBest(mi3, k=1000))

fi_tSVD_100  = ('fi', TruncatedSVD(n_components=100))
fi_tSVD_1000 = ('fi', TruncatedSVD(n_components=1000))

#endregion

#region --- Pipelines ---------------------------------------------------------

ovr_l_count_vectorizer_pipeline =                   Pipeline([count_vectorizer, ovr_l_svc])
ovr_l_svc_1gram_pipeline =                          Pipeline([tfidf_vectorizer_1gram, ovr_l_svc])
ovr_l_svc_2gram_pipeline =                          Pipeline([tfidf_vectorizer_2gram, ovr_l_svc])
ovr_l_svc_3gram_pipeline =                          Pipeline([tfidf_vectorizer_3gram, ovr_l_svc])
ovr_l_svc_2_3gram_pipeline =                        Pipeline([tfidf_vectorizer_2_3gram, ovr_l_svc])
ovr_l_svc_2gram_chi2_pipeline =                     Pipeline([tfidf_vectorizer_2gram, fi_chi2_1000, ovr_l_svc])

ovr_svc_1gram_pipeline =                            Pipeline([tfidf_vectorizer_1gram, ovr_svc])
ovr_g_nb_1gram_pipeline =                           Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_g_nb])
ovr_m_nb_1gram_pipeline =                           Pipeline([tfidf_vectorizer_1gram, ovr_m_nb])
ovr_b_nb_1gram_pipeline =                           Pipeline([tfidf_vectorizer_1gram, ovr_b_nb])
ovr_svc_kernel_rbf_1gram_pipeline =                 Pipeline([tfidf_vectorizer_1gram, ovr_svc_kernel_rbf])
ovr_nn_1gram_pipeline =                             Pipeline([tfidf_vectorizer_1gram, ovr_nn])
ovr_rf_1gram_pipeline =                             Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_rf])
ovr_dt_pipeline =                                   Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_dt])
ovr_ab_pipeline =                                   Pipeline([tfidf_vectorizer_1gram, to_dense, ovr_ab])

ovr_dt_count_vectorizer_pipeline =                  Pipeline([count_vectorizer, to_dense, ovr_dt])

ovr_sgd_1gram_pipeline =                            Pipeline([tfidf_vectorizer_1gram, ovr_sgd])
ovr_sgd_max_df_05_pipeline =                        Pipeline([tfidf_vectorizer_max_df_05, ovr_sgd])
ovr_sgd_max_df_05_w_auto_pipeline =                 Pipeline([tfidf_vectorizer_max_df_05, ovr_sgd_w_auto])
ovr_sgd_max_df_count_vectorizer_pipeline =          Pipeline([count_vectorizer, ovr_sgd_w_auto])

ovr_l_svc_w_auto_pipeline =                         Pipeline([tfidf_vectorizer_1gram, ovr_l_svc_w_auto])
ovr_l_svc_w_auto_count_vectorizer_pipeline =        Pipeline([count_vectorizer, ovr_l_svc_w_auto])
ovr_l_svc_w_auto_count_vectorizer_chi2_pipeline =   Pipeline([count_vectorizer, fi_chi2_1000, ovr_l_svc_w_auto])
occ_l_svc_pipeline =                                Pipeline([tfidf_vectorizer_1gram, occ_l_svc])


besf_pipeline =                                     Pipeline([tfidf_vectorizer_1gram, ovr_best])
#endregion

# the list of test to execute
testers = [

    Tester("../results/logs/french/__best.log",
           """
                BEST fr
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=besf_pipeline),

    # Window Size - NGrams

    Tester("../results/logs/french/00_1gram_count_vectorizer.log",
           """
                Par document
                Count vectorizer
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline),

    Tester("../results/logs/french/00_1gram_count_vectorizer_stemmer.log",
           """
                Par document
                stemmer
                Count vectorizer
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, stemmer_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline),

    Tester("../results/logs/french/00_1gram_count_vectorizer_lemmatizer.log",
           """
                Par document
                lemmatizer
                Count vectorizer
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline),

    Tester("../results/logs/french/00_1gram_count_vectorizer_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Count vectorizer
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=False),

    Tester("../results/logs/french/00_1gram_count_vectorizer_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag (2)
                Count vectorizer
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_count_vectorizer_pipeline,
           force=False),

    Tester("../results/logs/french/01_1gram.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto_count_vectorizer.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto_count_vectorizer_lemmatizer.log",
           """
                Par document
                lemmatizer
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto_count_vectorizer_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto_count_vectorizer_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_pipeline),

    Tester("../results/logs/french/01_1gram_w_auto_count_vectorizer_chi2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
                Count vectorizer
                Chi2
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_count_vectorizer_chi2_pipeline),


    Tester("../results/logs/french/01b_1gram.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD))
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_pipeline),

    Tester("../results/logs/french/01b_1gram_count_vectorizer_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 1gram
                TFIDF vectorizer
                OneVsRest SGD (Stochastic Gradient Descent (SGD))
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_sgd_1gram_pipeline),

    Tester("../results/logs/french/02_2gram.log",
           """
                Par document
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline),

    Tester("../results/logs/french/02_2gram_lemmatizer.log",
           """
                Par document
                Lemmatizer
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline),

    Tester("../results/logs/french/02_2gram_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_svc_2gram_pipeline),

    Tester("../results/logs/french/02_2gram_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_svc_2gram_pipeline),

    Tester("../results/logs/french/02_2gram_chi2.log",
           """
                Par document
                Window Size - 1 & 2gram
                OneVsRest Linear SVC
                Chi2
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2gram_chi2_pipeline),

    Tester("../results/logs/french/03_3gram.log",
           """
                Par document
                Window Size - 1, 2 & 3gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_3gram_pipeline),

    Tester("../results/logs/french/04_2-3gram.log",
           """
                Par document
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline),

    Tester("../results/logs/french/04_2-3gram_lemmatizer_filtered_by_postag.log",
           """
                Par document
                lemmatizer filtered by postag
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_svc_2_3gram_pipeline),

    Tester("../results/logs/french/04_2-3gram_lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                lemmatizer filtered by postag 2
                Window Size - 2 & 3gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_svc_2_3gram_pipeline),

    Tester("../results/logs/french/11_per_quotation.log",
           """
                Par phrase
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_quotation_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline),

    Tester("../results/logs/french/11_per_quotation_w_auto.log",
           """
                Par phrase
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_quotation_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/12_per_document_only_coded_quotations.log",
           """
                Par document, mais seulement avec les phrases ayant un code
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_only_coded_quotations_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline),

    Tester("../results/logs/french/12_per_document_only_coded_quotations_w_auto.log",
           """
                Par document, mais seulement avec les phrases ayant un code
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_only_coded_quotations_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/12_per_document_only_coded_quotations_w_auto_lemmatizer_filtered_by_postag.log",
           """
                Par document, mais seulement avec les phrases ayant un code
                lemmatizer filtered by postag
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_only_coded_quotations_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/12_per_document_only_coded_quotations_w_auto_lemmatizer_filtered_by_postag_2.log",
           """
                Par document, mais seulement avec les phrases ayant un code
                lemmatizer filtered by postag 2
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_only_coded_quotations_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/21_2alpha_codes.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline),

    Tester("../results/logs/french/21_2alpha_codes_w_auto.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_2alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/21_2alpha_codes_w_auto_lemmatizer_filtered_by_postag.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                lemmatizer filtered by postag
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_2alpha_tag_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/21_2alpha_codes_w_auto_lemmatizer_filtered_by_postag_2.log",
           """
                Par document, mais seulement avec les deux premières lettres des codes
                lemmatizer filtered by postag_2
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_2alpha_tag_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_l_svc_w_auto_pipeline),


    Tester("../results/logs/french/22_3alpha_codes.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_1gram_pipeline),

    Tester("../results/logs/french/22_3alpha_codes_w_auto.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest Linear SVC
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_l_svc_w_auto_pipeline),

    Tester("../results/logs/french/23_3alpha_codes_SGD.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD))
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_1gram_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_maxDF_05.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD) with max DF = 0.5 & sublinear_tf)
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_maxDF_05_w_auto.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_05_w_auto_pipeline),


    Tester("../results/logs/french/24_3alpha_codes_SGD_count_vectorizer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_count_vectorizer_stemmer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                stemmer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter, stemmer_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_count_vectorizer_lemmatizer.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_count_vectorizer_lemmatizer_filterd_by_postag.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer filtered by postag
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline),

    Tester("../results/logs/french/24_3alpha_codes_SGD_count_vectorizer_lemmatizer_filterd_by_postag_2.log",
           """
                Par document, mais seulement avec les trois premières lettres des codes
                Window Size - 1gram
                OneVsRest SGD (Stochastic Gradient Descent (SGD)) with max DF = 0.5 & sublinear_tf
                Class Weight = Auto
                Count Vectorizer
                lemmatizer filtered by postag 2
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[only_3alpha_tag_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_sgd_max_df_count_vectorizer_pipeline),


    Tester("../results/logs/french/31_svc.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest kernel SVC
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_svc_1gram_pipeline),

    # Tester("../results/logs/french/31_svc_kernel_rbf.log",
    # """
    # Par document
    #             Window Size - 1gram
    #             OneVsRest kernel SVC rbf (Radial Basis Function)
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=ovr_svc_kernel_rbf_1gram_pipeline),

    Tester("../results/logs/french/31_rf.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Random Forest
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_rf_1gram_pipeline),

    # Tester("../results/logs/french/31_nn.log",
    #        """
    #             Par document
    #             Window Size - 1gram
    #             NearestNeighbors
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=ovr_nn_1gram_pipeline),

    # Tester("../results/logs/french/31_occ_l_svc.log",
    #        """
    #             Par document
    #             Window Size - 1gram
    #             OutputCodeClassifier
    #        """,
    #        stored_hu_document_adapter=per_document_stored_hu_adapter,
    #        document_adapters=[default_corpus_adapter],
    #        pipeline=occ_l_svc_pipeline),

    Tester("../results/logs/french/31_dt.log",
           """
                Par document
                Window Size - 1gram
                Decision Tree
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_dt_pipeline),

    Tester("../results/logs/french/32_g_NB.log",
           """
               Par document
               Window Size - 1gram
               OneVsRest Gaussian Naive Bayes
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_g_nb_1gram_pipeline),

    Tester("../results/logs/french/32_m_NB.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline),

    Tester("../results/logs/french/32_m_NB__lemmatizer_filtered_by_postag.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_m_nb_1gram_pipeline),

    Tester("../results/logs/french/32_m_NB__lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest MultiNominal Naive Bayes
                Lemmatizer filtered by postag 2
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_m_nb_1gram_pipeline),

    Tester("../results/logs/french/32_b_NB.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline),


    Tester("../results/logs/french/32_b_NB__lemmatizer_filtered_by_postag.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
                Lemmatizer filtered by postag
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter],
           pipeline=ovr_b_nb_1gram_pipeline),

    Tester("../results/logs/french/32_b_NB__lemmatizer_filtered_by_postag_2.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest Bernoulli Naive Bayes
                Lemmatizer filtered by postag
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=ovr_b_nb_1gram_pipeline),

    Tester("../results/logs/french/32_ab.log",
           """
                Par document
                Window Size - 1gram
                OneVsRest AdaBoost
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_ab_pipeline),


    Tester("../results/logs/french/41_dt_count_vectorizer.log",
           """
                Par document
                CountVectorizer
                Decision Tree
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=ovr_dt_count_vectorizer_pipeline),

    Tester("../results/logs/french/xx__l_svc_00009.log",
           """
                Par document
                CountVectorizer
                Linear SVC (C=0.0009)
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter],
           pipeline=Pipeline([count_vectorizer, ovr_l_svc_w_auto_c_00009])),

    Tester("../results/logs/french/xx__l_svc_00009__stemmer.log",
           """
                Par document
                Stemmer
                CountVectorizer
                Linear SVC (C=0.0009)
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, stemmer_corpus_adapter],
           pipeline=Pipeline([count_vectorizer, ovr_l_svc_w_auto_c_00009])),

    Tester("../results/logs/french/xx__l_svc_002__lemmatizer.log",
           """
                Par document
                Lemmatizer
                CountVectorizer
                Linear SVC (C=0.02)
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_corpus_adapter],
           pipeline=Pipeline([count_vectorizer, ovr_l_svc_w_auto_c_002])),

    Tester("../results/logs/french/xx__l_svc_002__lemmatizer_filtered.log",
           """
                Par document
                Lemmatizer filterd by posTag (2)
                CountVectorizer
                Linear SVC (C=0.02)
           """,
           stored_hu_document_adapter=per_document_stored_hu_adapter,
           document_adapters=[default_corpus_adapter, lemmatizer_filtered_corpus_adapter_2],
           pipeline=Pipeline([count_vectorizer, ovr_l_svc_w_auto_c_002])),
]


for tester in testers:
    tester.test()

with open("../results/logs_viewer/logs.json", 'w') as f:
    f.write('[\n\t"')
    f.write('",\n\t"'.join(
        '../' + t.filename + ".json" for t in testers
    ))
    f.write('"\n]')